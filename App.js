import React from 'react';
import {View, StatusBar} from 'react-native';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import SplashScreen from '@screens/SplashScreen';
import QuestionNavigator from './src/navigator/QuestionNavigator';
import MainNavigator from './src/navigator/MainNavigator';
import {getData} from '@utils/GlobalFunction';
import isEmpty from '@utils/isEmpty';
import Global from '@utils/GlobalValue';
import EStyleSheet from 'react-native-extended-stylesheet';
EStyleSheet.build({$rem: Global.Scale});

console.disableYellowBox = true;

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isQEnd: false,
      loading: true,
    };
    StatusBar.setHidden(true);
  }

  async UNSAFE_componentWillMount() {
    // getAllData();

    const QEnd = await getData('QEnd');
    if (!isEmpty(QEnd) && QEnd === '1') {
      this.setState({
        isQEnd: true,
      });
    }
    this.setState({
      loading: false,
    });
  }

  render() {
    if (this.state.loading) {
      return <View style={{flex: 1, backgroundColor: Global.C_PERIWINKLE}} />;
    }
    if (this.state.isQEnd) {
      return <MainNavigator />;
    } else {
      return <QuestionNavigator />;
    }
  }
}

const RootNavigator = createSwitchNavigator(
  {
    App: App,
    Splash: SplashScreen,
  },
  {
    initialRouteName: 'Splash',
  },
);

export default createAppContainer(RootNavigator);
