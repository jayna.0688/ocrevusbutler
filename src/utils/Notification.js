import BackgroundTask from "react-native-background-task";
var PushNotification = require("react-native-push-notification");
// import {PushNotification} from 'react-native-push-notification';
// import PushNotificationIOS from "@react-native-community/push-notification-ios";

/*/BackgroundTask.define(() => {
    console.log("Hello from a background task");
    PushNotification.localNotification(notifyObj);
    BackgroundTask.finish();
});*/

PushNotification.configure({
    // (optional) Called when Token is generated (iOS and Android)
    onRegister: function(token) {
        console.log("TOKEN:", token);
    },

    // (required) Called when a remote or local notification is opened or received
    onNotification: function(notification) {
        console.log("NOTIFICATION:", notification);

        // process the notification

        // required on iOS only (see fetchCompletionHandler docs: https://github.com/react-native-community/react-native-push-notification-ios)
        // notification.finish(PushNotificationIOS.FetchResult.NoData);
    },

    // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
    // senderID: "YOUR GCM (OR FCM) SENDER ID",

    // IOS ONLY (optional): default: all - Permissions to register.
    permissions: {
        alert: true,
        badge: true,
        sound: true
    },

    // Should the initial notification be popped automatically
    // default: true
    popInitialNotification: true,

    /**
     * (optional) default: true
     * - Specified if permissions (ios) and token (android and ios) will requested or not,
     * - if not, you must call PushNotificationsHandler.requestPermissions() later
     */
    requestPermissions: true
});

const checkStatus = async () => {
    const status = await BackgroundTask.statusAsync();

    if (status.available) {
        // Everything's fine
        return;
    }

    const reason = status.unavailableReason;
    if (reason === BackgroundTask.UNAVAILABLE_DENIED) {
        Alert.alert(
            "Denied",
            'Please enable background "Background App Refresh" for this app'
        );
    } else if (reason === BackgroundTask.UNAVAILABLE_RESTRICTED) {
        Alert.alert(
            "Restricted",
            "Background tasks are restricted on your device"
        );
    }
};

export function ShowNotification() {
    const notifyObj = {
        foreground: true, // BOOLEAN: If the notification was received in foreground or not
        userInteraction: false, // BOOLEAN: If the notification was opened by the user from the notification area or not
        message: "My Notification Message", // STRING: The notification message
        data: {} // OBJECT: The push data
    };

    PushNotification.localNotification(notifyObj);

    // PushNotification.localNotificationSchedule({
    //     message: "My Notification Message", // (required)
    //     // date: new Date() // in 60 secs
    //     date: new Date(Date.now() + 10 * 1000)
    // });

    // BackgroundTask.schedule({
    //     period: 1200, // Aim to run every 20 mins - more conservative on battery
    // })

    checkStatus();
}
