import AsyncStorage from '@react-native-community/async-storage';

export function saveData(key, value) {
  try {
    AsyncStorage.setItem(key, value);
  } catch (e) {
    console.log('error', e);
  }
}
export async function getData(key) {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      return value;
    }
  } catch (e) {
    console.log('error', e);
  }
}

// test functions
export function deleteAllData() {
  try {
    AsyncStorage.clear();
  } catch (e) {
    console.log('error', e);
  }
}
export function deleteOneData(key) {
  try {
    AsyncStorage.removeItem(key);
  } catch (error) {
    console.log('error', error);
  }
}
export async function getAnswers() {
  try {
    let answers = [];
    for (let i = 0; i < 6; i++) {
      const ref = 'a' + (i + 1).toString();
      let a = await AsyncStorage.getItem(ref);
      a = a || '';
      answers.push(a);
    }
    return answers;
  } catch (error) {
    console.log('error', error);
  }
}
export async function getAllData() {
  try {
    const savedScreens = await AsyncStorage.getItem('savedScreens');
    const a1 = await AsyncStorage.getItem('a1');
    const a2 = await AsyncStorage.getItem('a2');
    const a3 = await AsyncStorage.getItem('a3');
    const a4 = await AsyncStorage.getItem('a4');
    const a5 = await AsyncStorage.getItem('a5');
    const a6 = await AsyncStorage.getItem('a6');
    const QEnd = await AsyncStorage.getItem('QEnd');
    const passGuide = await AsyncStorage.getItem('passGuide');
    const acceptAlarm = await AsyncStorage.getItem('acceptAlarm');
    const points = await AsyncStorage.getItem('points');

    // console.log('---------------storage-----------');
    // console.log('savedScreens', savedScreens);
    // console.log('a1', a1);
    // console.log('a2', a2);
    // console.log('a3', a3);
    // console.log('a4', a4);
    // console.log('a5', a5);
    // console.log('a6', a6);
    // console.log('QEnd', QEnd);
    // console.log('passGuide', passGuide);
    // console.log('acceptAlarm', acceptAlarm);
    // console.log('points', points);

    // // let pointdates = JSON.parse(points) || '';
    // const pointdates = points.substring(0, points.length - 1);
    // const pointDateArray = pointdates.split(':');
    // for (let i = 0; i < pointDateArray.length; i++) {
    //   const element = pointDateArray[i];
    //   const one = await AsyncStorage.getItem(element);
    //   console.log(element, one);
    // }
  } catch (e) {
    console.log('error', e);
  }
}

export function formatDateToString(year, month, date) {
  // 2019 0~11 1~31 => 20191130
  let year_str,
    month_str,
    date_str = '';
  year_str = year.toString();
  month_str = month < 9 ? '0' + (month + 1).toString() : (month + 1).toString();
  date_str = date < 10 ? '0' + date.toString() : date.toString();
  const dateString = year_str + month_str + date_str;
  return dateString;
}

export function formatDateFromSlash(slashdate) {
  // 11/30/2019 => 20191130
  const date =
    slashdate.substring(6, 10) +
    slashdate.substring(0, 2) +
    slashdate.substring(3, 5);
  return date;
}

export function formatTimeToString(time, minute) {
  // 0~23 0~59 => 0517
  let time_str,
    minute_str = '';
  time_str = time < 10 ? '0' + time.toString() : time.toString();
  minute_str = minute < 10 ? '0' + minute.toString() : minute.toString();
  const timeString = time_str + minute_str;
  return timeString;
}
