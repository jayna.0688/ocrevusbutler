import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Global from '@utils/GlobalValue';
import EStyleSheet from 'react-native-extended-stylesheet';
import Strings from '@utils/Strings';
const Button_Height = Math.round(Global.ScreenHeight * 0.1);
const BorderRadius = Math.round(Button_Height / 2);


const RoundButton = ({ onPress, label }) => {
  let Button_Width = 0;
  if (label === Strings.Continue || label === Strings.ShowApp) {
    Button_Width = Math.round(Global.ScreenWidth * 0.7);
  } else if (label === Strings.Finish) {
    Button_Width = Math.round(Global.ScreenWidth * 0.9);
  } else {
    Button_Width = Math.round(Global.ScreenWidth * 0.5);
  }
  return (
    <TouchableOpacity onPress={onPress} style={[styles.button_wrapper, { width: Button_Width }]}>
      <Text style={styles.button_text}>{label}</Text>
    </TouchableOpacity>
  );
}

export { RoundButton };


const styles = EStyleSheet.create({
  button_wrapper: {
    height: Button_Height,
    borderRadius: BorderRadius,
    backgroundColor: Global.C_YELLOW,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button_text: {
    fontFamily: Global.F_Avenir_Bold,
    fontWeight: 'bold',
    fontSize: '20rem',
    color: Global.C_BTNTEXT,
    lineHeight: Button_Height,
    letterSpacing: 2,
  },
});
