import React from 'react';
import {
    View,
    StyleSheet,
    TextInput,
    Modal,
    TouchableOpacity,
    Image,
} from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import { RoundButton } from '@component/Button';
import Strings from '@utils/Strings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
const ModalOkHeight = Math.round(Global.ScreenHeight * 0.09);
const Space = Math.round(Global.ScreenHeight * 0.06);

export default class InputCode extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            code: '123',
            modalVisible: false,
        };
    }

    componentWillUnmount() {
        this.setState({ modalVisible: false });
    }

    pressContinue = () => {
        if (this.state.code === '123') {
            this.props.navigation.navigate('QuestionScreen');
        } else {
            this.setState({ modalVisible: true });
        }
    };

    pressModalOk = () => {
        this.setState({ modalVisible: false });
    };

    renderModal() {
        return (
            <Modal
                visible={this.state.modalVisible}
                animationType={'fade'}
                onRequestClose={() => {
                    this.state.modalVisible(false);
                }}
                transparent
                style={{ flex: 1 }}>
                <TouchableOpacity
                    onPress={() => {
                        this.setState({ modalVisible: false });
                    }}
                    style={styles.modal_screenwrapper}>
                    <View style={styles.modal_wrapper}>
                        <View style={styles.modal_textwrapper}>
                            <TextS1
                                family="demi"
                                size="15"
                                color={Global.C_SUBTEXT}
                                label={Strings.CodeWrong}
                                textstyle={{ textAlign: 'center' }}
                            />
                        </View>

                        <View style={styles.modal_line}></View>

                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ modalVisible: false });
                            }}
                            style={styles.modal_ok}>
                            <TextS1
                                family="demi"
                                size="20"
                                color={Global.C_SUBTEXT}
                                label={'OK'}
                                textstyle={{ lineHeight: ModalOkHeight }}
                            />
                        </TouchableOpacity>
                    </View>
                </TouchableOpacity>
            </Modal>
        );
    }

    render() {
        if (this.state.loading) {
            return <View style={styles.container} />;
        }

        return (
            <KeyboardAwareScrollView
                contentContainerStyle={styles.container}
                scrollEnabled={false}>
                {/* menu modal */}
                {this.renderModal()}

                <View style={styles.subcontainer}>
                    <View style={styles.message_wrapper}>
                        <TextS1
                            family="demi"
                            size="16"
                            color={Global.C_PERIWINKLE}
                            label={Strings.CodeConfirm}
                            textstyle={{ textAlign: 'center' }}
                        />
                    </View>

                    <View style={styles.box_wrapper}>
                        <Image
                            source={require('@images/code_box.png')}
                            style={styles.wh100}
                            resizeMode="contain"
                        />
                    </View>

                    <View style={styles.codeinput_wrapper}>
                        <TextInput
                            autoCorrect={false}
                            autoCapitalize="none"
                            underlineColorAndroid="transparent"
                            multiline={false}
                            maxLength={6}
                            textAlignVertical="bottom"
                            keyboardType={'number-pad'}
                            style={styles.codeinput}
                            onChangeText={text => this.setState({ code: text })}
                            value={this.state.code}
                            placeholder="Code"
                            placeholderTextColor="darkgray"
                        />
                    </View>

                    <View style={styles.subtext_wrapper}>
                        <TextS1
                            family="demi"
                            size="16"
                            color={'#889DFB'}
                            label={'Ich habe kein Willkommenspaket'}
                            textstyle={{ textAlign: 'center' }}
                        />
                    </View>
                </View>

                <View style={styles.startbtn_wrapper}>
                    <RoundButton label={'WEITER'} onPress={this.pressContinue} />
                </View>
            </KeyboardAwareScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    subcontainer: {
        width: '100%',
        height: Math.round(Global.ScreenHeight * 0.8),
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    modal_screenwrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#cccccc88',
    },
    modal_wrapper: {
        position: 'absolute',
        width: '85%',
        backgroundColor: 'white',
        borderRadius: 5,
        flexDirection: 'column',
    },
    message_wrapper: {
        width: '80%',
    },
    startbtn_wrapper: {
        position: 'absolute',
        bottom: Math.round(Global.ScreenHeight * 0.1),
    },
    codeinput_wrapper: {
        width: '90%',
        marginTop: Space,
        borderBottomColor: 'darkgray',
        borderBottomWidth: 1,
    },
    codeinput: {
        fontFamily: Global.F_Avenir_Reg,
        fontSize: 30,
        color: Global.C_PERIWINKLE,
        fontWeight: 'normal',
        textAlign: 'center',
        letterSpacing: 10,
    },
    box_wrapper: {
        width: '40%',
        aspectRatio: 1.317,
        marginTop: Space,
    },
    wh100: {
        width: '100%',
        height: '100%',
    },
    subtext_wrapper: {
        marginTop: Space + 20,
    },
    modal_textwrapper: {
        width: '85%',
        alignSelf: 'center',
        marginVertical: 50,
    },
    modal_line: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray',
    },
    modal_ok: {
        width: '90%',
        alignSelf: 'center',
        height: ModalOkHeight,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
