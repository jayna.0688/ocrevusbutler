import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Animated,
  Easing,
} from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import { saveData, getData, getAnswers } from '@utils/GlobalFunction';
import OcticonsIcon from 'react-native-vector-icons/Octicons';
import Answer1View from './Answer1View';
import Answer2View from './Answer2View';
import Answer3View from './Answer3View';
import Answer4View from './Answer4View';
import Answer5View from './Answer5View';
import Answer6View from './Answer6View';
import ItemsGridView from './GridView';
import { RoundButton } from '@component/Button';
import AnswerImageItem from './AnswerImageItem';
import Strings from '@utils/Strings';
const QSelectItemWidth = Global.AnswerItemWidth;
const QTitle1 = [
  'An welchem Datum soll Deine',
  'Was ist Dein Geschlecht?',
  'Wie alt bist Du?',
  'Welche Form von MS',
  'Wann hast du deine MS-',
  'Welche Therapie(n)',
];
const QTitle2 = [
  'erste Ocrevus®-Infusion',
  '',
  '',
  'hast Du?',
  'Diagnose erhalten?',
  'hast Du bislang erhalten?',
];


export default class QuestionScreen extends React.Component {
  constructor(props) {
    super(props);
    const { params } = props.navigation.state;
    this.state = {
      showGridView: false,
      doValidateGridView: false,
      savedFirstAnswer: false,

      screenIndex: 1, // 1~6
      savedScreens: (params && params.savedScreens) || '', // '1534'
      allSaved: false,
      showContinueMessage: false,
      loading: true,
      answers: [], // [6]
    };
    this.onChanged = this.onChanged.bind(this);
    this.onPressGridViewItem = this.onPressGridViewItem.bind(this);
    this.animatedValue = new Animated.Value(0);
    this.pressFinish = this.pressFinish.bind(this);

  }

  UNSAFE_componentWillMount = () => {
    let screenIndex = 1;
    while (this.state.savedScreens.indexOf(`${screenIndex}`) !== -1) {
      screenIndex++;
    }
    if (screenIndex > 6) {
      this.setState({
        screenIndex: 6,
        allSaved: true,
        loading: false,
        savedFirstAnswer: true
      });
    } else {
      if (screenIndex > 1) {
        this.setState({
          savedFirstAnswer: true,
        });
      }
      this.setState({
        screenIndex: screenIndex,
        loading: false,
      });
      if (this.state.savedScreens.length > 0) {
        this.startAnimationOfContinueMessage();
      }
    }
    this.getAnswersFromStorage();
  };

  async getAnswersFromStorage() {
    const answers = await getAnswers() || [];
    this.setState({
      answers: answers,
    })
  }

  startAnimationOfContinueMessage() {
    this.setState({
      showContinueMessage: true,
    });
    this.animatedValue.setValue(0.01);
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 5000,
      easing: Easing.linear,
      delay: 500,
    }).start(() => {
      this.setState({
        showContinueMessage: false,
      });
    });
  }

  renderQSelectItems() {
    const screenIndex = this.state.screenIndex;
    let circleLocation = 2; // 1~3
    let secondScreenIndex = screenIndex;
    if (screenIndex === 1) {
      circleLocation = 1;
      secondScreenIndex = 2;
    } else if (screenIndex === 6) {
      circleLocation = 3;
      secondScreenIndex = 5;
    }

    let oneScreenSaved = [];
    const savedScreens = this.state.savedScreens;
    for (let i = 0; i < 3; i++) {
      if (savedScreens.indexOf(`${secondScreenIndex - 1 + i}`) !== -1) {
        oneScreenSaved[i] = true;
      } else {
        oneScreenSaved[i] = false;
      }
    }

    let groupQItems = [];
    for (let i = 1; i < 4; i++) {
      const borderWidth = circleLocation === i ? 4 : 0;
      const screenIndex = secondScreenIndex + i - 2;
      const imageContentData = this.state.answers[screenIndex - 1];
      groupQItems.push(
        <TouchableOpacity
          key={i}
          onPress={this.pressQScreenItem.bind(this, i)}
          style={[styles.qselection_one, { borderWidth: borderWidth }]}>
          {oneScreenSaved[i - 1] ? (
            // style - qselection_one_image
            <View style={{ width: '70%', height: '70%', justifyContent: 'center', alignItems: 'center' }}>
              <AnswerImageItem screenIndex={screenIndex} data={imageContentData} />
            </View>
          ) : (
              <View style={{ height: 32 }}>
                <TextS2
                  family="bold"
                  size="30"
                  color={'white'}
                  label={screenIndex}
                  textstyle={{ lineHeight: 32 }}
                />
              </View>
            )}
        </TouchableOpacity>,
      );
    }

    return <View style={styles.qselection}>{groupQItems}</View>;
  }
  pressQScreenItem(index) {
    // 1~3
    if (!this.state.savedFirstAnswer) {
      return;
    }
    let screenIndex = this.state.screenIndex;
    if (index === 1) {
      if (screenIndex === 6) {
        screenIndex = 4;
      } else if (screenIndex === 1) {
        return;
      } else {
        screenIndex--;
      }
    } else if (index === 2) {
      if (screenIndex === 6) {
        screenIndex = 5;
      } else if (screenIndex === 1) {
        screenIndex = 2;
      }
    } else {
      if (screenIndex === 6) {
        return;
      } else if (screenIndex === 1) {
        screenIndex = 3;
      } else {
        screenIndex++;
      }
    }
    this.setState({
      screenIndex: screenIndex,
    });
  }

  onChanged(data) {
    const screen = data.screen;
    if (screen === 1) {
      this.setState({
        savedFirstAnswer: true,
      });
    }
    let savedScreens = this.state.savedScreens;
    if (savedScreens.indexOf(`${screen}`) === -1) {
      savedScreens += screen;
      saveData('savedScreens', savedScreens);
      this.checkAllSaved(savedScreens);
      this.setState({
        savedScreens: savedScreens,
      });
    }
    const key = 'a' + screen.toString();
    const value = data.save.toString();
    saveData(key, value);

    let answers = this.state.answers;
    answers[screen - 1] = value;
    this.setState({
      answers: answers,
    })
  }

  checkAllSaved(savedScreens) {
    if (savedScreens.length > 5) {
      this.setState({
        allSaved: true,
      });
    }
  }

  renderAnswerView() {
    const screenIndex = this.state.screenIndex;
    if (screenIndex === 1) {
      return <Answer1View onChanged={this.onChanged} />;
    } else if (screenIndex === 2) {
      return <Answer2View onChanged={this.onChanged} />;
    } else if (screenIndex === 3) {
      return <Answer3View onChanged={this.onChanged} />;
    } else if (screenIndex === 4) {
      return <Answer4View onChanged={this.onChanged} />;
    } else if (screenIndex === 5) {
      return <Answer5View onChanged={this.onChanged} />;
    } else if (screenIndex === 6) {
      return <Answer6View onChanged={this.onChanged} />;
    } else {
      return <View />;
    }
  }

  renderStatusIcon() {
    const showGridView = this.state.showGridView;
    const lefticon_width = showGridView ? 25 : 35;
    const righticon_oneline_width = showGridView ? 24 : 15;
    const righticon_oneline_height = showGridView ? 12 : 8;
    return (
      <TouchableOpacity
        disabled={!this.state.savedFirstAnswer}
        onPress={() =>
          this.setState({
            showGridView: !showGridView,
            doValidateGridView: false,
          })
        }
        style={styles.topicon}>
        <View
          style={[
            styles.status_lefticon,
            {
              width: lefticon_width,
            },
          ]}>
          <OcticonsIcon
            name="primitive-dot"
            size={showGridView ? 7 : 10}
            color={'white'}
          />
          <OcticonsIcon
            name="primitive-dot"
            size={showGridView ? 13 : 20}
            color={'white'}
          />
          <OcticonsIcon
            name="primitive-dot"
            size={showGridView ? 7 : 10}
            color={'white'}
          />
        </View>

        <View style={styles.status_righticon}>
          <View
            style={[
              styles.status_righticon_oneline,
              {
                width: righticon_oneline_width,
                height: righticon_oneline_height,
              },
            ]}>
            <OcticonsIcon
              name="primitive-dot"
              size={showGridView ? 15 : 7}
              color={'white'}
            />
            <OcticonsIcon
              name="primitive-dot"
              size={showGridView ? 15 : 7}
              color={'white'}
            />
          </View>
          <View
            style={[
              styles.status_righticon_oneline,
              {
                width: righticon_oneline_width,
                height: righticon_oneline_height,
              },
            ]}>
            <OcticonsIcon
              name="primitive-dot"
              size={showGridView ? 15 : 7}
              color={'white'}
            />
            <OcticonsIcon
              name="primitive-dot"
              size={showGridView ? 15 : 7}
              color={'white'}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  onPressGridViewItem(screenIndex) {
    this.setState({
      showGridView: false,
      screenIndex: screenIndex,
      doValidateGridView: false,
    });
  }

  pressFinish = (isFinishBtn) => { // finish or continue
    if (isFinishBtn) {
      const savedScreens = this.state.savedScreens;
      if (savedScreens.length === 6) {
        this.props.navigation.navigate('FinishScreen');
      } else {
        this.setState({
          doValidateGridView: true,
        });
      }
    } else { // continue
      let screenIndex = this.state.screenIndex;
      if (screenIndex === 1 && !this.state.savedFirstAnswer) return;
      if (screenIndex === 6) screenIndex = 0;
      this.setState({
        screenIndex: screenIndex + 1
      })
    }
  };


  render() {
    if (this.state.loading) {
      return <View style={styles.container} />;
    }

    const screenIndex = this.state.screenIndex;
    let ContinueMessageLeft = -500;
    if (this.state.showContinueMessage) {
      ContinueMessageLeft = this.animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [Global.ScreenWidth + 50, -300],
      });
    }
    const noBottomButton = !this.state.showGridView && (screenIndex === 6 && !this.state.allSaved);
    // const answerViewFlex = (!this.state.allSaved && screenIndex === 6) ? 0.57 : 0.42;
    const answerViewFlex = noBottomButton ? 0.57 : 0.42;
    const isFinishBtn = this.state.allSaved || this.state.showGridView;
    const bottombtn_text = isFinishBtn ? Strings.Finish : Strings.Continue;

    return (
      <View style={styles.container}>

        {/* scene type selector */}
        <View style={styles.topicon_wrapper}>{this.renderStatusIcon()}</View>

        {/* question grid items */}
        {this.state.showGridView ?
          <View style={styles.f_075}>
            <ItemsGridView
              onPressGridViewItem={this.onPressGridViewItem}
              savedScreens={this.state.savedScreens}
              answers={this.state.answers}
              doValidateGridView={this.state.doValidateGridView}
            />
          </View>
          :
          <View style={styles.qselection_wrapper}>
            {this.renderQSelectItems()}
          </View>
        }

        {!this.state.showGridView && <View style={styles.f_003} />}

        {/* title */}
        {!this.state.showGridView && (
          <View style={styles.title_wrapper}>
            <TextS2
              family="bold"
              size="20"
              color={Global.C_PERIWINKLE}
              label={QTitle1[screenIndex - 1]}
            />
            <TextS2
              family="bold"
              size="20"
              color={Global.C_PERIWINKLE}
              label={QTitle2[screenIndex - 1]}
            />
          </View>
        )}

        {/* answer content */}
        {!this.state.showGridView && (
          <View
            style={{
              flex: answerViewFlex,
            }}>
            {this.renderAnswerView()}
          </View>
        )}

        {/* finish button */}
        {!noBottomButton &&
          <View style={[styles.finishbtn_wrapper, { backgroundColor: this.state.showGridView ? Global.C_PERIWINKLE_LIGHT : 'white' }]}>
            <RoundButton label={bottombtn_text} onPress={() => this.pressFinish(isFinishBtn)} />
          </View>
        }

        {/* continue message */}
        {this.state.showContinueMessage && (
          <Animated.View
            style={[styles.continuemessage, { left: ContinueMessageLeft }]}>
            <TextS2
              family="bold"
              size="20"
              color={Global.C_PERIWINKLE}
              label={'please continue setting'}
            />
          </Animated.View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: 'white',
    flexDirection: 'column',
  },
  topicon_wrapper: {
    flex: 0.09,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingRight: 20,
    backgroundColor: Global.C_PERIWINKLE_LIGHT
  },
  qselection_wrapper: {
    flex: 0.21,
    alignItems: 'center',
    backgroundColor: Global.C_PERIWINKLE_LIGHT
  },
  f_075: {
    flex: 0.75,
  },
  f_003: {
    flex: 0.03,
  },
  title_wrapper: {
    zIndex: 10,
    flex: 0.09,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  finishbtn_wrapper: {
    flex: 0.16,
    width: '100%',
    alignItems: 'center',
  },
  continuemessage: {
    position: 'absolute',
    bottom: 5,
  },
  topicon: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  qselection: {
    width: '90%',
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  qselection_one: {
    width: QSelectItemWidth,
    height: QSelectItemWidth,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'white',
    borderRadius: Math.round(QSelectItemWidth / 2),
  },
  qselection_one_image: {
    width: '50%',
    height: '50%',
    justifyContent: 'center',
    alignItems: 'center',

  },
  wh100: {
    width: '100%',
    height: '100%',
  },
  status_lefticon: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  status_righticon: {
    marginLeft: 15,
    flexDirection: 'column',
  },
  status_righticon_oneline: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});
