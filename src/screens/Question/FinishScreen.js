import React from 'react';
import { View, StyleSheet } from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import RNRestart from 'react-native-restart';
import { RoundButton } from '@component/Button';
import {
  saveData,
} from '@utils/GlobalFunction';
import Strings from '@utils/Strings';

export default class FinishScreen extends React.Component {
  pressEnter() {
    saveData('QEnd', '1');
    RNRestart.Restart();
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.desc_wrapper}>
          <TextS2
            family="bold"
            size="28"
            color={Global.C_PERIWINKLE}
            label="Geschafft!"
          />

          <TextS1
            family="reg"
            size="17"
            color={Global.C_PERIWINKLE}
            label={Strings.FinishMessage}
            textstyle={styles.desc_text}
          />
        </View>

        <View style={styles.enterbtn_wrapper}>
          <RoundButton label={Strings.ShowApp} onPress={this.pressEnter} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  enterbtn_wrapper: {
    position: 'absolute',
    bottom: Math.round(Global.ScreenHeight * 0.1),
  },
  desc_wrapper: {
    width: '85%',
    height: '90%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  desc_text: {
    textAlign: 'center',
    marginTop: 50,
  },
});