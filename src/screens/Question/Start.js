import React from 'react';
import { View, StyleSheet } from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import { getData } from '@utils/GlobalFunction';
import { NavigationActions } from 'react-navigation';
import isEmpty from '@utils/isEmpty';
import { RoundButton } from '@component/Button';
import Strings from '@utils/Strings';

export default class StartScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  async UNSAFE_componentWillMount() {
    const savedScreens = await getData('savedScreens');
    if (!isEmpty(savedScreens)) {
      const navigateAction = NavigationActions.navigate({
        routeName: 'QuestionScreen',
        params: {
          savedScreens: savedScreens,
        },
      });
      this.props.navigation.dispatch(navigateAction);
      this.pressStart = this.pressStart.bind(this);
    } else {
      this.setState({
        loading: false,
      });
    }
  }

  pressStart = () => {
    this.props.navigation.navigate('InputCodeScreen');
  };

  render() {
    if (this.state.loading) {
      return <View style={styles.container} />;
    }

    return (
      <View style={styles.container}>

        <View style={styles.hallo_wrapper}>
          <TextS2
            family="bold"
            size="28"
            color={Global.C_PERIWINKLE}
            label="Schön, dass du"
          />
          <TextS2
            family="bold"
            size="28"
            color={Global.C_PERIWINKLE}
            label="da bist!"
          />
          <TextS1
            family="demi"
            size="16"
            color={Global.C_PERIWINKLE}
            label={Strings.StartMessage}
            textstyle={{ textAlign: 'center', marginTop: 30 }}
          />
        </View>

        <View style={styles.startbtn_wrapper}>
          <RoundButton label={Strings.Start} onPress={this.pressStart} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  hallo_wrapper: {
    width: '75%',
    height: Math.round(Global.ScreenHeight * 0.8),
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  startbtn_wrapper: {
    position: 'absolute',
    bottom: Math.round(Global.ScreenHeight * 0.1),
  },
});
