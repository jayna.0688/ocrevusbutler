import React from 'react';
import {
    View,
    TouchableOpacity,
    StyleSheet,
    FlatList,
    Image,
} from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import ArrowTextHeader from '@component/ArrowTextHeader';
import { NavigationActions } from 'react-navigation';
const Items = [
    {
        image: require('@images/uber_image.png'),
        content: 'Blutuntersuchungen bei OCREVUS®',
        desc: 'Dein Arzt untersucht vor Beginn der Therapie mit OCREVUS® …lesen',
    },
    {
        image: require('@images/uber_image.png'),
        content: 'Blutuntersuchungen bei OCREVUS®',
        desc: 'Dein Arzt untersucht vor Beginn der Therapie mit OCREVUS® …lesen',
    },
];

// test
const ArticleData = [
    {
        // vaccination
        image: require('@images/moreinfo_vaccination.png'),
        title: 'Impfen bei OCREVUS® – geht das?',
        desc:
            'Weder die MS noch die OCREVUS® Therapie verhindern, dass Du Dich impfen lassen kannst. Im Gegenteil: Ein guter Impfschutz ist für Dich besonders wichtig!',
        infolist: [
            {
                title: 'Warum überhaupt impfen lassen?',
                text:
                    'Verschiedene Infektionskrankheiten wie beispielsweise die Grippe können bei MS-Betroffenen Schübe auslösen. Zudem kann die Grippe bei ihnen einen schwereren Verlauf nehmen. Auch die Therapie mit OCREVUS® kann Dich anfälliger für Infektionen machen. Hintergrund ist die Wirkweise (interner LINK): Die fehlgeleiteten B-Zellen, die OCREVUS® aus dem Körper entfernt, sind auch an der Immunantwort beteiligt. Impfungen können Dich vor einer Ansteckung schützen. Die Behauptung, Impfungen könnten einen Schub oder gar MS auslösen, wird durch Studien widerlegt.',
            },
            {
                title: 'Besonderheiten beim Impfen unter OCREVUS®',
                text:
                    'Ein vor Therapiebeginn vom Immunsystem aufgebauter Schutz gegen Krankheitserreger bleibt unter OCREVUS® erhalten. Die immunmodulierende Wirkung von OCREVUS® kann jedoch dazu führen, dass Dein Körper während der Behandlung nicht ganz so gut auf Impfungen anspricht. Aber auch bei bereits begonnener Therapie kannst Du geimpft werden. So kannst Du beispielsweise weiterhin die jährliche Grippeimpfung erhalten. Denn selbst wenn die Impfantwort möglicherweise geringer ausfällt, kann sie das Infektionsrisiko reduzieren.',
            },
        ],
    },
];

export default class UberOcrevus extends React.Component {

    pressItem() {
        const navigateAction = NavigationActions.navigate({
            routeName: 'ArticleScreen',
            params: {
                data: ArticleData[0],
            },
        });
        this.props.navigation.dispatch(navigateAction);
    }

    renderItem(item) {
        console.log('---item', item);
        return (
            <View style={styles.item_container}>
                {/* image */}
                <TouchableOpacity
                    onPress={this.pressItem.bind(this)}
                    style={styles.item_imagewrapper}>
                    <Image
                        source={item.image}
                        style={styles.wh100}
                        resizeMode="contain"
                    />
                    <View style={styles.item_content}>
                        <TextS1
                            family="demi"
                            size="15"
                            color={'white'}
                            label={item.content}
                        />
                    </View>
                </TouchableOpacity>

                {/* desc */}
                <View style={styles.item_descwrapper}>
                    <TextS1
                        family="demi"
                        size="14"
                        color={Global.C_SUBTEXT}
                        label={item.desc}
                    />
                </View>
            </View>
        );
    }

    renderFooter() {
        return <View style={styles.footer}></View>;
    }

    render() {
        return (
            <View style={styles.container}>
                {/* header */}
                <ArrowTextHeader
                    title={'Über OCREVUS®'}
                    onPress={() => this.props.navigation.goBack()}
                />

                <FlatList
                    data={Items}
                    renderItem={({ item }) => this.renderItem(item)}
                    keyExtractor={(item, index) => index.toString()}
                    ListFooterComponent={this.renderFooter}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: Global.ScreenWidth,
        height: Global.ScreenHeight,
        alignItems: 'center',
    },
    footer: {
        height: 80,
    },
    item_container: {
        marginTop: 30,
        width: Global.ScreenWidth,
        flexDirection: 'column',
        alignItems: 'center',
    },
    wh100: {
        width: '100%',
        height: '100%',
    },
    item_imagewrapper: {
        width: '90%',
        aspectRatio: 1.91,
    },
    item_descwrapper: {
        width: '80%',
        marginVertical: 10,
    },
});
