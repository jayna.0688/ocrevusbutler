import React from 'react';
import {
    View,
    TouchableOpacity,
    StyleSheet,
    Image,
    TextInput,
    Platform
} from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import {
    formatDateToString,
    formatTimeToString,
    formatDateFromSlash,
} from '@utils/GlobalFunction';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import RectShadowImage from '@component/RectShadowImage';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DateTimePicker from '@react-native-community/datetimepicker';
const ViewWidth = Math.round(Global.ScreenWidth * 0.7);
const ViewLeft = Math.round((Global.ScreenWidth - ViewWidth) / 2);
const ItemImages = [
    require('@images/inject.png'),
    require('@images/flask.png'),
    require('@images/plus.png'),
];
const C_BACK_ADDDESCRIPTION = '#8C95BF';

export default class AddNewView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stage: 1, // 1~2
            addtype: 0, // 1~3
            date: '', // 20191130
            time: '', // 0517
            desc: '',
            datetime_value: new Date(),
            datetime_mode: 'date',
            datetime_show: false,
            todayString: '',
        };
    }


    componentDidMount() {
        const today = new Date();
        const todayString = formatDateToString(today.getFullYear(), today.getMonth(), today.getDate());
        const initDate = this.props.initDate; // 11/30/2019
        const year = parseInt(initDate.substring(6, 10), 10);
        const month = parseInt(initDate.substring(0, 2), 10) - 1;
        const day = parseInt(initDate.substring(3, 5), 10);
        const newDate = new Date(year, month, day);
        const date = formatDateToString(year, month, day);
        this.setState({
            date: date,
            time: '0830',
            datetime_value: newDate,
            todayString: todayString,
        })
    }

    pressClose() {
        this.props.onClose();
    }

    pressItem(index) {
        // 1~3
        this.setState({
            stage: 2,
            addtype: index,
        });
    }

    renderAddNew() {
        return (
            <View style={styles.addnew_container}>
                <View style={styles.addnew_closewrapper}>
                    <TouchableOpacity
                        onPress={this.pressClose.bind(this)}
                        style={styles.addnew_close}>
                        <AntDesignIcon name="close" size={15} color={'white'} />
                    </TouchableOpacity>
                </View>

                <View style={styles.addnew_desc}>
                    <TextS1
                        family="reg"
                        size="15"
                        color={'white'}
                        label={'Welchen Termin möchtest du eintragen?'}
                        textstyle={{ textAlign: 'center' }}
                    />
                </View>

                <View style={styles.addnew_threeitems}>
                    <View style={styles.addnew_threeitems_wrapper}>
                        <View style={styles.addnew_oneitem}>
                            <RectShadowImage
                                width={'100%'}
                                image={ItemImages[0]}
                                onPress={this.pressItem.bind(this, 1)}
                                backcolor={Global.C_GRAYBACK}
                            />
                            <TextS0
                                family="reg"
                                size="12"
                                color={'white'}
                                label={'Impfung'}
                                textstyle={styles.addnew_oneitemtext}
                            />
                        </View>

                        <View style={styles.addnew_oneitem}>
                            <RectShadowImage
                                width={'100%'}
                                image={ItemImages[1]}
                                onPress={this.pressItem.bind(this, 2)}
                                backcolor={Global.C_GRAYBACK}
                            />
                            <TextS0
                                family="reg"
                                size="12"
                                color={'white'}
                                label={'Blutunter-suchung'}
                                textstyle={styles.addnew_oneitemtext}
                            />
                        </View>

                        <View style={styles.addnew_oneitem}>
                            <RectShadowImage
                                width={'100%'}
                                image={ItemImages[2]}
                                onPress={this.pressItem.bind(this, 3)}
                                backcolor={Global.C_GRAYBACK}
                            />
                            <TextS0
                                family="reg"
                                size="12"
                                color={'white'}
                                label={'Anderer Termin'}
                                textstyle={styles.addnew_oneitemtext}
                            />
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    pressDateTime(isDate) {
        this.setState({
            datetime_mode: isDate ? 'date' : 'time',
            datetime_show: true,
        })
    }

    pressSave() {
        const { date, time, desc, addtype } = this.state;
        if (desc === '') return;

        this.props.onSave(date, addtype, time, desc);
        return;
    }

    setDateTime = (event, datetime) => {
        if (datetime === undefined) return; // cancel
        const { datetime_value, datetime_mode } = this.state;
        const temp = datetime || datetime_value;
        if (datetime_mode === 'date') {
            const dateString = formatDateToString(temp.getFullYear(), temp.getMonth(), temp.getDate());
            this.setState({
                date: dateString,
                datetime_show: false,
                datetime_value: temp,
            })
        } else {
            const timeString = formatTimeToString(temp.getHours(), temp.getMinutes());
            this.setState({
                time: timeString,
                datetime_show: false,
                datetime_value: temp,
            })
        }
    }

    renderAddNewProperty() {
        let { datetime_value, datetime_mode, datetime_show, date, time, todayString } = this.state; // 20191130 0517
        // datetime_show = true;
        let display_date, display_time = '';
        const month = parseInt(date.substring(4, 6), 10) - 1;
        display_date = date.substring(6, 8) + ' ' + Global.Months[month].toUpperCase() + ' ' + date.substring(0, 4);
        display_time = time.substring(0, 2) + ':' + time.substring(2, 4) + ' Uhr';

        let isToday = false;
        if (display_date === todayString) isToday = true;

        return (
            <View style={styles.addblood_wrapper}>
                <View style={styles.addblood_closewrapper}>
                    <TouchableOpacity
                        onPress={this.pressClose.bind(this)}
                        style={styles.addblood_close}>
                        <AntDesignIcon name="close" size={15} color={'white'} />
                    </TouchableOpacity>
                </View>

                <View style={styles.add_titleinput}>
                    <View style={styles.addproperty_titlewrapper}>
                        <TextS0
                            family="reg"
                            size="18"
                            color={'white'}
                            label={'Welcher Termin?'}
                        />
                    </View>

                </View>

                <View style={styles.addother_dateselector}>
                    <View style={styles.addblood_dateselectorwrapper}>
                        <TouchableOpacity onPress={this.pressDateTime.bind(this, true)} style={styles.addblood_dateselector_date}>
                            <TextS0
                                family="reg"
                                size="12"
                                color={'white'}
                                label={isToday ? 'Heute' : display_date}
                                textstyle={styles.mr10}
                            />
                            <AntDesignIcon name="caretdown" size={10} color={'white'} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={this.pressDateTime.bind(this, false)} style={styles.addblood_dateselector_time}>
                            <TextS0
                                family="reg"
                                size="12"
                                color={'white'}
                                label={display_time}
                                textstyle={styles.mr10}
                            />
                            <AntDesignIcon name="caretdown" size={10} color={'white'} />
                        </TouchableOpacity>


                        {datetime_show &&
                            <DateTimePicker
                                value={datetime_value}
                                mode={datetime_mode}
                                is24Hour={true}
                                display="default"
                                onChange={this.setDateTime} />
                        }

                    </View>
                </View>

                <View style={styles.addother_descinputwrapper}>
                    <View style={styles.addother_descsubwrapper}>
                        <TextS0
                            family="reg"
                            size="12"
                            color={'white'}
                            label={'Beschreibung'}
                            textstyle={styles.mt20}
                        />
                        <TextInput
                            autoCorrect={false}
                            multiline={true}
                            numberOfLines={3}
                            autoCapitalize={'sentences'}
                            underlineColorAndroid="transparent"
                            maxLength={150}
                            textAlignVertical="bottom"
                            style={styles.addother_descinput}
                            onChangeText={text => this.setState({ desc: text })}
                            value={this.state.desc}
                            placeholder=""
                        />
                    </View>
                </View>

                <View style={styles.addother_savewrapper}>
                    <TouchableOpacity onPress={this.pressSave.bind(this)} style={styles.addother_save}>
                        <TextS0
                            family="demi"
                            size="12"
                            color={'white'}
                            label={'SPEICHERN'}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    render() {
        const stage = this.state.stage;
        const isToday = this.props.isToday;

        let ViewTop = 0;
        if (isToday) {
            ViewTop = Math.round(Global.ScreenHeight * 0.39) + 60;
        } else {
            ViewTop = Math.round(Global.ScreenHeight * 0.31) + 60;
        }

        return (
            <KeyboardAwareScrollView
                style={styles.container}
                contentContainerStyle={styles.subcontainer}
                scrollEnabled={false}>
                <View style={[styles.modalcontainer, { top: ViewTop }]}>
                    {stage === 1 ? this.renderAddNew() : this.renderAddNewProperty()}
                </View>
            </KeyboardAwareScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: 20,
        left: 0,
        top: 0,
        backgroundColor: 'transparent',
    },
    subcontainer: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
    },
    modalcontainer: {
        position: 'absolute',
        width: ViewWidth,
        aspectRatio: 1,
        left: ViewLeft,
        backgroundColor: Global.C_PERIWINKLE,
        borderRadius: 5,
    },
    addnew_container: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
    },
    addnew_closewrapper: {
        flex: 0.15,
        width: '100%',
        alignItems: 'flex-end',
    },
    addnew_close: {
        height: '100%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    addnew_desc: {
        flex: 0.3,
        width: '80%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    addnew_threeitems: {
        flex: 0.55,
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    addnew_threeitems_wrapper: {
        width: '90%',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    addnew_oneitem: {
        width: '30%',
        flexDirection: 'column',
        alignItems: 'center',
    },
    addnew_oneitemtext: {
        marginTop: 10,
        textAlign: 'center',
    },
    addblood_wrapper: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
    },
    addblood_closewrapper: {
        flex: 0.15,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    addblood_close: {
        height: '100%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    add_titleinput: {
        flex: 0.15,
        marginHorizontal: 30,
        justifyContent: 'center',
        // alignItems:'center'
    },
    addproperty_titlewrapper: {
        borderBottomColor: 'white',
        borderBottomWidth: 1,
        justifyContent: 'flex-end'
    },
    addblood_dateselector: {
        flex: 0.1,
        width: '100%',
        paddingHorizontal: 30,
        justifyContent: 'flex-end',
    },
    addblood_dateselectorwrapper: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    addblood_dateselector_date: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    addblood_dateselector_time: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 20,
    },
    mr10: {
        marginRight: 10,
    },
    addother_dateselector: {
        flex: 0.13,
        width: '100%',
        paddingHorizontal: 30,
        justifyContent: 'center',
    },
    addblood_imagewrapper: {
        flex: 0.46,
        marginHorizontal: 20,
    },
    addblood_imagesubwrapper: {
        width: '100%',
        marginTop: 20,
        flexDirection: 'row',
    },
    addblood_imagetext: {
        marginLeft: 15,
        width: '60%',
    },
    addblood_buttonwrapper: {
        flex: 0.14,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    addblood_delete: {
        marginRight: 10,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    addblood_save: {
        marginRight: 20,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    addother_titleinputtext: {
        fontFamily: Global.F_Avenir_Reg,
        fontSize: 18,
        fontWeight: 'normal',
        color: 'white',
        borderBottomColor: 'white',
        borderBottomWidth: 1,
    },
    addother_descinputwrapper: {
        flex: 0.43,
        marginHorizontal: 20,
        justifyContent: 'center',
    },
    addother_descsubwrapper: {
        width: '100%',
        height: '80%',
        backgroundColor: C_BACK_ADDDESCRIPTION,
        borderRadius: 5,
        flexDirection: 'column',
        justifyContent: 'center',
        paddingHorizontal: 15,
    },
    mt20: {
        marginTop: 20,
    },
    addother_descinput: {
        fontFamily: Global.F_Avenir_Reg,
        fontSize: 12,
        fontWeight: 'normal',
        color: 'white',
    },
    addother_savewrapper: {
        flex: 0.14,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    addother_save: {
        marginRight: 30,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    picker_control: {
        width: 0,
        height: 0
    },
});