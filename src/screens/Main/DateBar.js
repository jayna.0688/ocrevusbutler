import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import OcticonsIcon from 'react-native-vector-icons/Octicons';
const pickDateViewWidth = Math.round(Global.ScreenWidth * 0.17);
const ArrowWidth = Math.round(Global.ScreenWidth * 0.125);
const OneDateWidth = Math.round(Global.ScreenWidth * 0.25);
const PickDatePaddingLeft = Math.round((OneDateWidth - pickDateViewWidth) / 2);
const C_SAVEDINDICATOR = '#444F7F';

export default class DateBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    pressDate(index) {
        // 0~2
        this.props.onChangeDateIndex(index);
    }

    pressArrow(isNext) {
        this.props.onChangeDateArrow(isNext);
    }

    render() {
        const pointDates = this.props.pointDates;
        const dates = this.props.dates || []; // ['11/16/2019', ...]
        const pickIndex = this.props.pickIndex;
        if (dates.length !== 3) return <View></View>;
        let dateGroup = [];
        for (let i = 0; i < 3; i++) {
            const date = dates[i].substring(3, 5);

            let showIndicator = false;
            const mergedate = dates[i].substring(6, 10) + dates[i].substring(0, 2) + dates[i].substring(3, 5);
            if (pointDates.includes(mergedate)) showIndicator = true;

            dateGroup.push(
                <View key={i} style={styles.onedate_wrapper}>
                    <TouchableOpacity
                        onPress={this.pressDate.bind(this, i)}
                        style={styles.onedate}>
                        <View>
                            <TextS2 family="reg" size="15" color="white" label={date} />

                            {showIndicator &&
                                <View style={styles.onedate_indicator}>
                                    <OcticonsIcon
                                        name="primitive-dot"
                                        size={15}
                                        color={C_SAVEDINDICATOR}
                                    />
                                </View>
                            }
                        </View>
                    </TouchableOpacity>
                </View>,
            );
        }
        const pickDateLeft =
            ArrowWidth + PickDatePaddingLeft + pickIndex * OneDateWidth;
        const pickDate = dates[pickIndex].substring(3, 5);
        const pickMonth = Global.Months[
            parseInt(dates[pickIndex].substring(0, 2), 10) - 1
        ].toUpperCase();

        return (
            <View style={styles.container}>
                <View style={styles.mainarea}>
                    {/* left arrow */}
                    <TouchableOpacity
                        onPress={this.pressArrow.bind(this, false)}
                        style={styles.arrow}>
                        <EntypoIcon name="chevron-left" size={20} color={'white'} />
                    </TouchableOpacity>

                    {/* date bar */}
                    {dateGroup}

                    {/* right arrow */}
                    <TouchableOpacity
                        onPress={this.pressArrow.bind(this, true)}
                        style={styles.arrow}>
                        <EntypoIcon name="chevron-right" size={20} color={'white'} />
                    </TouchableOpacity>
                </View>

                {/* shadow */}
                <View style={styles.shadow_wrapper}>
                    <View style={styles.shadow}></View>
                </View>

                {/* pickDate */}
                <View style={[styles.datepicker, { left: pickDateLeft }]}>
                    <TextS1
                        family="demi"
                        size="20"
                        color={Global.C_PERIWINKLE}
                        label={pickDate}
                    />
                    <TextS0
                        family="demi"
                        size="12"
                        color={Global.C_PERIWINKLE}
                        label={pickMonth}
                        textstyle={styles.mt5}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
    },
    mainarea: {
        zIndex: 10,
        width: '100%',
        height: '65%',
        backgroundColor: Global.C_PERIWINKLE_LIGHT,
        flexDirection: 'row',
    },
    arrow: {
        zIndex: 10,
        flex: 0.125,
        justifyContent: 'center',
        alignItems: 'center',
    },
    shadow_wrapper: {
        zIndex: 8,
        top: -2,
    },
    shadow: {
        width: '100%',
        height: 2,
        backgroundColor: 'white',
        elevation: 20,
        shadowColor: Global.C_PERIWINKLE,
        shadowOpacity: 0.6,
        shadowOffset: { width: 0, height: 10 },
        shadowRadius: 10,
    },
    datepicker: {
        zIndex: 11,
        position: 'absolute',
        width: pickDateViewWidth,
        aspectRatio: 0.8,
        top: 0,
        backgroundColor: Global.C_GRAYBACK,
        borderRadius: 3,
        elevation: 10,
        shadowColor: Global.C_PERIWINKLE,
        shadowOpacity: 0.6,
        shadowOffset: { width: 0, height: 5 },
        shadowRadius: 10,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    mt5: {
        marginTop: 5,
    },
    onedate_wrapper: {
        zIndex: 10,
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    onedate: {
        width: '80%',
        height: '80%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    onedate_indicator: {
        position: 'absolute',
        top: -8,
        right: -8,
    },
});
