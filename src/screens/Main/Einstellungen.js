import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import ArrowTextHeader from '@component/ArrowTextHeader';
const ItemImages = [
    require('@images/select_off.png'),
    require('@images/select_on.png'),
];
const HeaderText =
    'Damit Du auf dem Laufenden bleibst und nichts vergisst, möchte ich Dich gerne an Deine anstehenden Termine erinnern. Bist Du damit einverstanden?';

export default class Einstellungen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            select: false,
        };
    }

    render() {
        return (
            <View style={styles.container}>
                {/* header */}
                <ArrowTextHeader
                    title={'Einstellungen'}
                    onPress={() => this.props.navigation.goBack()}
                />

                <View style={styles.desc_wrapper}>
                    <TextS1
                        family="demi"
                        size="14"
                        color={Global.C_SUBTEXT}
                        label={HeaderText}
                        textstyle={{ textAlign: 'center' }}
                    />
                </View>

                <View style={styles.selector_wrapper}>
                    <TextS1
                        family="bold"
                        size="16"
                        color={Global.C_SUBTEXT}
                        label={'  BENACHRICHTIGUNGEN'}
                    />

                    <TouchableOpacity
                        onPress={() => this.setState({ select: !this.state.select })}
                        style={styles.selector}>
                        <Image
                            source={this.state.select ? ItemImages[1] : ItemImages[0]}
                            style={styles.wh100}
                            resizeMode="contain"
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    desc_wrapper: {
        width: '75%',
        marginVertical: 40,
    },
    selector_wrapper: {
        width: '80%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    selector: {
        width: 50,
        aspectRatio: 2,
    },
    wh100: {
        width: '100%',
        height: '100%',
    },
});
