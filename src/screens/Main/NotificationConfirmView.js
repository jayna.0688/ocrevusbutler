import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import {
    saveData,
} from '@utils/GlobalFunction';
import Strings from '../../utils/Strings';
const Button_Height = Math.round(Global.ScreenHeight * 0.1);

export default class NotificationConfirmView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    pressYesNo(isYes) {
        if (isYes) {
            saveData('acceptAlarm', '1');
        } else {
            saveData('acceptAlarm', '0');
        }
        this.props.onFinish();
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.subcontainer}>
                    <TextS1
                        family="demi"
                        size="14"
                        color={Global.C_SUBTEXT}
                        label={Strings.NotificationAlarm}
                        textstyle={styles.desc}
                    />

                    <View style={styles.buttons}>
                        <TouchableOpacity
                            onPress={this.pressYesNo.bind(this, false)}
                            style={styles.button_no}>
                            <TextS1
                                family="demi"
                                size="16"
                                color={Global.C_SUBTEXT}
                                label={'NEIN'}
                                textstyle={{ lineHeight: Button_Height }}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={this.pressYesNo.bind(this, true)}
                            style={styles.button_yes}>
                            <TextS1
                                family="demi"
                                size="16"
                                color={Global.C_SUBTEXT}
                                label={'JA'}
                                textstyle={{ lineHeight: Button_Height }}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        zIndex: 20,
        position: 'absolute',
        width: '100%',
        height: '100%',
        left: 0,
        top: 0,
        backgroundColor: '#ffffff99',
        justifyContent: 'center',
        alignItems: 'center',
    },
    subcontainer: {
        width: '80%',
        backgroundColor: 'white',
        borderRadius: 5,
        flexDirection: 'column',
        alignItems: 'center',
        elevation: 10,
        shadowColor: 'gray',
        shadowOpacity: 0.5,
        shadowOffset: { width: 0, height: 5 },
        shadowRadius: 20,
    },
    desc: {
        textAlign: 'center',
        marginVertical: 50,
        marginHorizontal: 40,
    },
    buttons: {
        width: '100%',
        height: Button_Height,
        borderTopColor: 'lightgray',
        borderTopWidth: 1,
        flexDirection: 'row',
    },
    button_no: {
        flex: 0.5,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    button_yes: {
        flex: 0.5,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderLeftColor: 'lightgray',
        borderLeftWidth: 1,
    },
});