import React from 'react';
import {
    View,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import {
    saveData,
    getData,
    formatDateToString,
    formatTimeToString,
    formatDateFromSlash
} from '@utils/GlobalFunction';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import RectShadowImage from '@component/RectShadowImage';
import DateTimePicker from '@react-native-community/datetimepicker';
const ViewWidth = Math.round(Global.ScreenWidth * 0.7);
const ViewLeft = Math.round((Global.ScreenWidth - ViewWidth) / 2);
const C_BACK_ADDDESCRIPTION = '#8C95BF';
const ItemImages = [
    require('@images/inject.png'),
    require('@images/flask.png'),
    ''
];

export default class PointEditView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 1,
            date: '', // 20191130
            time: '', // 0517
            desc: '',

            datetime_value: null,
            datetime_mode: 'date',
            datetime_show: false,

            loading: true,
        };
    }

    componentDidMount() {
        const date = formatDateFromSlash(this.props.editObj.date); // 11/30/2019
        const item = this.props.editObj.item;

        const year = parseInt(date.substring(0, 4), 10);
        const month = parseInt(date.substring(4, 6), 10) - 1;
        const day = parseInt(date.substring(6, 8), 10);
        const hour = parseInt(item.time.substring(0, 2), 10);
        const minute = parseInt(item.time.substring(2, 4), 10);

        const newDate = new Date(year, month, day, hour, minute);

        this.setState({
            date: date,
            time: item.time,
            desc: item.desc,
            type: item.type,
            datetime_value: newDate,
            loading: false,
        })
    }

    pressClose() {
        this.props.onClose();
    }

    async pressDelete() {
        this.props.onDelete(this.props.editObj.item);
    }

    pressDateTime(isDate) {
        this.setState({
            datetime_mode: isDate ? 'date' : 'time',
            datetime_show: true,
        })
    }

    pressSave() {

        const { date, time, desc, type } = this.state;
        if (desc === '') return;
        this.props.onSave(date, type, time, desc);
    }

    async savePointData(type, date, time, desc) {
        let points = await getData('points') || '';
        if (points.indexOf(`${date}`) === -1) {
            points += date + ':';
        }
        const tempObj = {
            time: time,
            type: type,
            desc: desc
        }
        saveData('points', points);
        saveData(date.toString(), JSON.stringify(tempObj));
        this.props.onSave(date, tempObj);
    }

    setDateTime = (event, datetime) => {
        if (datetime === undefined) return; // cancel
        const { datetime_value, datetime_mode } = this.state;
        const temp = datetime || datetime_value;
        if (datetime_mode === 'date') {
            const dateString = formatDateToString(temp.getFullYear(), temp.getMonth(), temp.getDate());
            this.setState({
                date: dateString,
                datetime_show: false,
                datetime_value: temp,
            })
        } else {
            const timeString = formatTimeToString(temp.getHours(), temp.getMinutes());
            this.setState({
                time: timeString,
                datetime_show: false,
                datetime_value: temp,
            })
        }
    }

    render() {

        if (this.state.loading) return <View></View>
        const isToday = this.props.isToday;
        let ViewTop = 0;
        if (isToday) {
            ViewTop = Math.round(Global.ScreenHeight * 0.39) + 60;
        } else {
            ViewTop = Math.round(Global.ScreenHeight * 0.31) + 60;
        }

        const { datetime_value, datetime_mode, datetime_show, date, time, type } = this.state; // 20191130 0517
        let display_date, display_time = '';
        const month = parseInt(date.substring(4, 6), 10) - 1;
        display_date = date.substring(6, 8) + ' ' + Global.Months[month].toUpperCase() + ' ' + date.substring(0, 4);
        display_time = time.substring(0, 2) + ':' + time.substring(2, 4) + ' Uhr';

        const title = type === "1" ? 'Impfung' : type === "2" ? 'Blutuntersuchung' : 'Other...';

        return (
            <View style={styles.container}>
                <View style={[styles.modalcontainer, { top: ViewTop }]}>

                    <View style={styles.addblood_wrapper}>
                        <View style={styles.addblood_closewrapper}>
                            <TouchableOpacity
                                onPress={this.pressClose.bind(this)}
                                style={styles.addblood_close}>
                                <AntDesignIcon name="close" size={15} color={'white'} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.add_titleinput}>
                            <TextS0
                                family="reg"
                                size="18"
                                color={'white'}
                                label={title}
                            />
                        </View>

                        {/* date selection */}
                        <View style={styles.addblood_dateselector}>
                            <View style={styles.addblood_dateselectorwrapper}>
                                <TouchableOpacity onPress={this.pressDateTime.bind(this, true)} style={styles.addblood_dateselector_date}>
                                    <TextS0
                                        family="reg"
                                        size="12"
                                        color={'white'}
                                        label={display_date}
                                        textstyle={styles.mr10}
                                    />
                                    <AntDesignIcon name="caretdown" size={10} color={'white'} />
                                </TouchableOpacity>

                                <TouchableOpacity onPress={this.pressDateTime.bind(this, false)} style={styles.addblood_dateselector_time}>
                                    <TextS0
                                        family="reg"
                                        size="12"
                                        color={'white'}
                                        label={display_time}
                                        textstyle={styles.mr10}
                                    />
                                    <AntDesignIcon name="caretdown" size={10} color={'white'} />
                                </TouchableOpacity>

                                {datetime_show &&
                                    <DateTimePicker
                                        value={datetime_value}
                                        mode={datetime_mode}
                                        is24Hour={true}
                                        display="default"
                                        onChange={this.setDateTime} />
                                }
                            </View>
                        </View>

                        {/* image content */}
                        <View style={styles.addblood_imagewrapper}>
                            <View style={styles.addblood_imagesubwrapper}>
                                <RectShadowImage
                                    width={'30%'}
                                    image={ItemImages[type - 1]}
                                    backcolor={Global.C_GRAYBACK}
                                />
                                <TextS0
                                    family="reg"
                                    size="12"
                                    color={'white'}
                                    label={this.state.desc}
                                    textstyle={styles.addblood_imagetext}
                                />
                            </View>
                        </View>

                        {/* delete save */}
                        <View style={styles.addblood_buttonwrapper}>
                            <TouchableOpacity onPress={this.pressDelete.bind(this)} style={styles.addblood_delete}>
                                <TextS0
                                    family="demi"
                                    size="10"
                                    color={'white'}
                                    label={'LÖSCHEN'}
                                />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={this.pressSave.bind(this)} style={styles.addblood_save}>
                                <TextS0
                                    family="demi"
                                    size="10"
                                    color={'white'}
                                    label={'SPEICHERN'}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        zIndex: 20,
        position: 'absolute',
        width: '100%',
        height: '100%',
        left: 0,
        top: 0,
        backgroundColor: '#ffffff99',
        alignItems: 'center',
    },
    modalcontainer: {
        position: 'absolute',
        width: ViewWidth,
        aspectRatio: 1,
        left: ViewLeft,
        backgroundColor: Global.C_PERIWINKLE,
        borderRadius: 5,
    },
    addnew_container: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
    },
    addnew_closewrapper: {
        flex: 0.15,
        width: '100%',
        alignItems: 'flex-end',
    },
    addnew_close: {
        height: '100%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    addnew_desc: {
        flex: 0.3,
        width: '80%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    addnew_threeitems: {
        flex: 0.55,
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    addnew_threeitems_wrapper: {
        width: '90%',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    addnew_oneitem: {
        width: '30%',
        flexDirection: 'column',
        alignItems: 'center',
    },
    addnew_oneitemtext: {
        marginTop: 10,
        textAlign: 'center',
    },
    addblood_wrapper: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
    },
    addblood_closewrapper: {
        flex: 0.15,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    addblood_close: {
        height: '100%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    add_titleinput: {
        flex: 0.15,
        marginHorizontal: 30,
    },
    addblood_dateselector: {
        flex: 0.1,
        width: '100%',
        paddingHorizontal: 30,
        justifyContent: 'flex-end',
    },
    addblood_dateselectorwrapper: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    addblood_dateselector_date: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    addblood_dateselector_time: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 20,
    },
    mr10: {
        marginRight: 10,
    },
    addother_dateselector: {
        flex: 0.13,
        width: '100%',
        paddingHorizontal: 30,
        justifyContent: 'flex-end',
    },
    addblood_imagewrapper: {
        flex: 0.46,
        marginHorizontal: 20,
    },
    addblood_imagesubwrapper: {
        width: '100%',
        marginTop: 20,
        flexDirection: 'row',
    },
    addblood_imagetext: {
        marginLeft: 15,
        width: '60%',
    },
    addblood_buttonwrapper: {
        flex: 0.14,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    addblood_delete: {
        marginRight: 10,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    addblood_save: {
        marginRight: 20,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    addother_titleinputtext: {
        fontFamily: Global.F_Avenir_Reg,
        fontSize: 18,
        fontWeight: 'normal',
        color: 'white',
        borderBottomColor: 'white',
        borderBottomWidth: 1,
    },
    addother_descinputwrapper: {
        flex: 0.43,
        marginHorizontal: 20,
        justifyContent: 'center',
    },
    addother_descsubwrapper: {
        width: '100%',
        height: '80%',
        backgroundColor: C_BACK_ADDDESCRIPTION,
        borderRadius: 5,
        flexDirection: 'column',
        justifyContent: 'center',
        paddingHorizontal: 15,
    },
    mt20: {
        marginTop: 20,
    },
    addother_descinput: {
        fontFamily: Global.F_Avenir_Reg,
        fontSize: 12,
        fontWeight: 'normal',
        color: 'white',
    },
    addother_savewrapper: {
        flex: 0.14,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    addother_save: {
        marginRight: 30,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
});
