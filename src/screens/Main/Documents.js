import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import ArrowTextHeader from '@component/ArrowTextHeader';
import RectShadowImage from '@component/RectShadowImage';
const itemRectWidth = Math.round(Global.ScreenWidth * 0.4);
const ItemImages = [
    require('@images/inject.png'),
    require('@images/flask.png'),
];
const HeaderTxt =
    'Hier findest Du eine Zusammenfassung aller Checklisten für Deine Termine. Bitte vergiss nicht, dass Du alle Checklisten vor Deinem Therapietag erledigen musst.\n\nIch helfe Dir dabei.';

export default class Documents extends React.Component {
    constructor(props) {
        super(props);
        this.pressItem = this.pressItem.bind(this);
    }

    pressItem(index) { // 1~2
        const navigateAction = NavigationActions.navigate({
            routeName: 'ChecklistScreen',
            params: {
                type: index
            }
        });
        this.props.navigation.dispatch(navigateAction);
    }

    render() {
        return (
            <View style={styles.container}>
                {/* header */}
                <ArrowTextHeader
                    title={'Dokumente'}
                    onPress={() => this.props.navigation.goBack()}
                />

                <View style={styles.desc}>
                    <TextS0
                        family="demi"
                        size="14"
                        color={Global.C_SUBTEXT}
                        label={HeaderTxt}
                        textstyle={{ textAlign: 'center' }}
                    />
                </View>

                <View style={styles.mv15}>
                    <RectShadowImage
                        width={itemRectWidth}
                        image={ItemImages[0]}
                        backcolor={Global.C_PERIWINKLE}
                        onPress={() => this.pressItem(1)}
                    />
                </View>

                <View style={styles.mv15}>
                    <RectShadowImage
                        width={itemRectWidth}
                        image={ItemImages[1]}
                        backcolor={Global.C_PERIWINKLE}
                        onPress={() => this.pressItem(2)}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    desc: {
        width: '80%',
        marginVertical: 40,
    },
    mv15: {
        marginVertical: 15,
    },
});
