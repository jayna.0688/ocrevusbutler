import React from 'react';
import {View, TouchableOpacity, StyleSheet, Image} from 'react-native';
import {TextS0, TextS1, TextS2} from '@component/Text';
import Global from '@utils/GlobalValue';
const ItemImages = [
  require('@images/icon-drug.png'),
  require('@images/icon-blood.png'),
  require('@images/icon-pre.png'),
  require('@images/icon_therapy.png'),
];
const ItemImagesSelect = [
  require('@images/inject_select.png'),
  require('@images/icon-blood-drop.png'),
  require('@images/icon-pre-select.png'),
  require('@images/icon_therapy.png'),
];

const ImageWrapperWidth = Math.round(Global.ScreenHeight * 0.13);

export default class TreatLineView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      IconsLeftArray: [],
    };
  }

  componentDidMount() {
    this.setState({
      index: 0,
      IconsLeftArray: [
        0,
        Global.ScreenWidth * 0.3,
        Global.ScreenWidth * 0.6,
        Global.ScreenWidth - ImageWrapperWidth,
      ],
    });
  }

  pressIcon(index) {
    // 0~3
    const selectindex = index === 3 ? 0 : index + 1;
    this.setState({
      index: selectindex,
    });
    this.props.onOverView(index);
  }

  render() {
    const WrapperLeft = this.state.IconsLeftArray;

    return (
      <View style={styles.container}>
        <View style={styles.linewrapper}>
          <View style={styles.line}></View>
        </View>

        <View style={styles.items_wrapper}>
          {ItemImages.map((item, index) => {
            const isSelected = this.props.initTreatlineSelect
              ? false
              : this.state.index === index + 1;
            const image = isSelected ? ItemImagesSelect[index] : item;
            if (isSelected) {
              return (
                <View
                  key={index}
                  style={[styles.item_wrapper, {left: WrapperLeft[index]}]}>
                  <View
                    style={{
                      width: '80%',
                      borderRadius: Math.round(ImageWrapperWidth * 0.4),
                      aspectRatio: 1,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                      elevation: 10,
                      shadowColor: Global.C_PERIWINKLE,
                      shadowOpacity: 0.6,
                      shadowOffset: {width: 0, height: 0},
                      shadowRadius: 10,
                    }}>
                    <TouchableOpacity
                      onPress={this.pressIcon.bind(this, index)}
                      style={styles.item_small_select}>
                      <Image
                        source={image}
                        style={[
                          styles.wh100,
                          {tintColor: Global.C_YELLOWGREEN},
                        ]}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              );
            } else {
              return (
                <View
                  key={index}
                  style={[styles.item_wrapper, {left: WrapperLeft[index]}]}>
                  <TouchableOpacity
                    onPress={this.pressIcon.bind(this, index)}
                    style={styles.item_small}>
                    <Image
                      source={item}
                      style={styles.wh100}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>
              );
            }
          })}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
  },
  linewrapper: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  line: {
    width: '80%',
    height: 2,
    backgroundColor: Global.C_YELLOWGREEN,
  },
  items_wrapper: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  item_wrapper: {
    position: 'absolute',
    height: '100%',
    aspectRatio: 1,
    top: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  item_small: {
    height: '40%',
    aspectRatio: 1,
    backgroundColor: 'white',
  },
  item_small_select: {
    height: '60%',
    aspectRatio: 1,
    backgroundColor: 'white',
  },
  wh100: {
    width: '100%',
    height: '100%',
  },
});
