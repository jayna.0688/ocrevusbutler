import React from 'react';
import {
    View,
    TouchableOpacity,
    Image,
    StyleSheet,
    ScrollView,
} from 'react-native';
import Global from '@utils/GlobalValue';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';

const MoreInfoVaccinationImage = '@images/moreinfo_vaccination.png';
const CloseImage = '@images/icon_close.png';
const CloseBtnWidth = Math.round(Global.ScreenWidth * 0.1);
const HeaderHeight = Math.round(Global.ScreenHeight * 0.15);
const title = 'Impfen bei OCREVUS® – geht das?';
const desc =
    'Weder die MS noch die OCREVUS® Therapie verhindern, dass Du Dich impfen lassen kannst. Im Gegenteil: Ein guter Impfschutz ist für Dich besonders wichtig!';
const infolist = [
    {
        title: 'Warum überhaupt impfen lassen?',
        text:
            'Verschiedene Infektionskrankheiten wie beispielsweise die Grippe können bei MS-Betroffenen Schübe auslösen. Zudem kann die Grippe bei ihnen einen schwereren Verlauf nehmen. Auch die Therapie mit OCREVUS® kann Dich anfälliger für Infektionen machen. Hintergrund ist die Wirkweise (interner LINK): Die fehlgeleiteten B-Zellen, die OCREVUS® aus dem Körper entfernt, sind auch an der Immunantwort beteiligt. Impfungen können Dich vor einer Ansteckung schützen. Die Behauptung, Impfungen könnten einen Schub oder gar MS auslösen, wird durch Studien widerlegt.',
    },
    {
        title: 'Besonderheiten beim Impfen unter OCREVUS®',
        text:
            'Ein vor Therapiebeginn vom Immunsystem aufgebauter Schutz gegen Krankheitserreger bleibt unter OCREVUS® erhalten. Die immunmodulierende Wirkung von OCREVUS® kann jedoch dazu führen, dass Dein Körper während der Behandlung nicht ganz so gut auf Impfungen anspricht. Aber auch bei bereits begonnener Therapie kannst Du geimpft werden. So kannst Du beispielsweise weiterhin die jährliche Grippeimpfung erhalten. Denn selbst wenn die Impfantwort möglicherweise geringer ausfällt, kann sie das Infektionsrisiko reduzieren.',
    },
];

export default class Article extends React.Component {
    constructor(props) {
        super(props);
        const { params } = props.navigation.state;
        this.state = {
            image: params.data.image || '',
            title: params.data.title || '',
            desc: params.data.desc || '',
            infolist: params.data.infolist || [],
        };
    }

    render() {
        const image = this.state.image;

        return (
            <View style={styles.container}>
                {/* absolute top image */}
                <View style={styles.top_image}>
                    {image !== '' && (
                        <Image source={image} style={styles.wh100} resizeMode="contain" />
                    )}
                    {/* close button */}
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                        style={styles.closebtn_wrapper}>
                        <Image
                            source={require(CloseImage)}
                            style={styles.closebtn}
                            resizeMode="contain"
                        />
                    </TouchableOpacity>
                </View>

                {/* absolute */}
                <ScrollView
                    style={styles.maincontainer}
                    contentContainerStyle={styles.maincontainercontent}>
                    {image !== '' && (
                        <View style={styles.overlappedimage}>
                            <Image
                                source={require(MoreInfoVaccinationImage)}
                                style={styles.wh100}
                                resizeMode="contain"
                            />
                        </View>
                    )}

                    <View
                        style={[
                            styles.mainsubcontainer,
                            { top: image === '' ? 0 : -HeaderHeight },
                        ]}>
                        <View style={styles.title}>
                            <TextS0
                                family="bold"
                                size="24"
                                color={Global.C_PERIWINKLE}
                                label={title}
                            />
                        </View>

                        <View style={styles.desc}>
                            <TextS0
                                family="reg"
                                size="16"
                                color={Global.C_SUBTEXT}
                                label={desc}
                            />
                        </View>

                        {infolist.map((item, index) => {
                            return (
                                <View key={index} style={styles.item_wrapper}>
                                    {/* top line */}
                                    <View style={styles.topline}></View>

                                    <View style={styles.item_titlewrapper}>
                                        <View style={styles.w90}>
                                            <TextS0
                                                family="reg"
                                                size="20"
                                                color={Global.C_PERIWINKLE}
                                                label={item.title}
                                            />
                                        </View>

                                        <TouchableOpacity style={styles.item_upbutton}>
                                            <IoniconsIcon
                                                name="ios-arrow-up"
                                                size={20}
                                                color={Global.C_PERIWINKLE}
                                            />
                                        </TouchableOpacity>
                                    </View>

                                    <View style={styles.item_text}>
                                        <TextS0
                                            family="reg"
                                            size="16"
                                            color={Global.C_SUBTEXT}
                                            label={item.text}
                                        />
                                    </View>
                                </View>
                            );
                        })}

                        {/* line */}
                        <View style={styles.bottomline}></View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
        flexDirection: 'column',
    },
    top_image: {
        zIndex: 0,
        width: '100%',
        aspectRatio: 1.778,
    },
    wh100: {
        width: '100%',
        height: '100%',
    },
    closebtn_wrapper: {
        position: 'absolute',
        width: CloseBtnWidth,
        height: CloseBtnWidth,
        right: CloseBtnWidth - 10,
        top: CloseBtnWidth,
        backgroundColor: 'white',
        borderRadius: Math.round(CloseBtnWidth / 2),
        justifyContent: 'center',
        alignItems: 'center',
    },
    closebtn: {
        width: '60%',
        height: '60%',
        tintColor: Global.C_PERIWINKLE,
    },
    maincontainer: {
        zIndex: 1,
        position: 'absolute',
        left: 0,
        top: HeaderHeight,
        width: Global.ScreenWidth,
        height: Global.ScreenHeight - HeaderHeight,
        flexDirection: 'column',
        backgroundColor: 'white',
    },
    maincontainercontent: {
        alignItems: 'center',
    },
    overlappedimage: {
        top: -HeaderHeight,
        width: '100%',
        aspectRatio: 1.778,
    },
    mainsubcontainer: {
        width: '100%',
        flexDirection: 'column',
        alignItems: 'center',
    },
    title: {
        marginVertical: 30,
        width: '80%',
    },
    desc: {
        marginBottom: 40,
        width: '80%',
    },
    item_wrapper: {
        width: '100%',
        flexDirection: 'column',
        alignItems: 'center',
    },
    topline: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray',
        marginBottom: 30,
    },
    bottomline: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray',
        marginBottom: 50,
    },
    item_titlewrapper: {
        width: '100%',
        paddingLeft: '10%',
        paddingRight: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        marginBottom: 20,
    },
    w90: {
        width: '90%',
    },
    item_upbutton: {
        width: 30,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    item_text: {
        width: '80%',
        marginBottom: 30,
    },
});
