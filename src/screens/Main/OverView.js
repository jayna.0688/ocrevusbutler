import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Image,
} from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
const title =
  'Morgen is es soweit! Deine Therapie startet. Hier findest Du ein paar Vorschläge, was Du mitnehmen kannst, um perfekt vorbereitet zu sein.';
const Items = [
  {
    image: require('@images/docs.png'),
    image_select: require('@images/docs_select.png'),
    title: 'Wichtige Dokumente',
    content: '\nBlutwerte\n\nImpfpass\n\nWeitere Unterlagen\n(z.B. MRT Bilder)',
  },
  {
    image: require('@images/clothes.png'),
    image_select: require('@images/clothes_select.png'),
    title: 'Für mehr Gemütlichkeit',
    content: '\nBequeme Kleidung\n\nKuschelsocken',
  },
  {
    image: require('@images/snacks.png'),
    image_select: require('@images/snacks_select.png'),
    title: 'Für den kleinen Hunger',
    content: '\nSnacks\n\nEtwas zu trinken',
  },
  {
    image: require('@images/devices.png'),
    image_select: require('@images/devices_select.png'),
    title: 'Für den Zeitvertreib',
    content: '\nEtwas zu lesen\n\nMusik\n\nTablet\n\nHandy und Ladekabel',
  },
];
const CheckHeight = 30;
const C_CIRCLE_BACK_DESELECT = '#dfe2f0';

export default class OverView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndexs: '',
    };
  }

  pressItem(index) {
    // 0~3
    let selectedIndexs = this.state.selectedIndexs;
    const inclueIndex = selectedIndexs.indexOf(index.toString());
    if (inclueIndex !== -1) {
      const tempString =
        selectedIndexs.substring(0, inclueIndex) +
        selectedIndexs.substring(inclueIndex + 1, selectedIndexs.length);
      selectedIndexs = tempString;
    } else {
      selectedIndexs += index.toString();
    }

    this.setState({
      selectedIndexs: selectedIndexs,
    });
  }

  renderListItem(item, index) {
    const placeLeft = index % 2 === 0;
    const selectedIndexs = this.state.selectedIndexs;

    const isSelected =
      selectedIndexs.indexOf(index.toString()) === -1 ? false : true;

    const check_backcolor = isSelected
      ? Global.C_PERIWINKLE
      : C_CIRCLE_BACK_DESELECT;
    return (
      <View style={styles.item_container}>
        {placeLeft && (
          <TouchableOpacity
            onPress={this.pressItem.bind(this, index)}
            style={styles.item_imagewrapper}>
            <Image
              source={isSelected ? item.image_select : item.image}
              style={styles.wh100}
              resizeMode="contain"
            />
            <View
              style={[
                styles.item_check,
                { backgroundColor: check_backcolor, right: 0 },
              ]}>
              <FontAwesomeIcon
                name="check"
                size={20}
                color={isSelected ? 'white' : Global.C_PERIWINKLE_MORELIGHT}
              />
            </View>
          </TouchableOpacity>
        )}

        <View style={styles.item_text}>
          <TextS0
            family="demi"
            size="18"
            color={Global.C_BTNTEXT}
            label={item.title}
            textstyle={{ textAlign: 'center' }}
          />

          <TextS0
            family="reg"
            size="14"
            color={Global.C_SUBTEXT}
            label={item.content}
            textstyle={{ textAlign: 'center' }}
          />
        </View>

        {!placeLeft && (
          <TouchableOpacity
            onPress={this.pressItem.bind(this, index)}
            style={styles.item_imagewrapper}>
            <Image
              source={isSelected ? item.image_select : item.image}
              style={styles.wh100}
              resizeMode="contain"
            />
            <View
              style={[
                styles.item_check,
                { left: 0, backgroundColor: check_backcolor },
              ]}>
              <FontAwesomeIcon
                name="check"
                size={20}
                color={isSelected ? 'white' : Global.C_PERIWINKLE_MORELIGHT}
              />
            </View>
          </TouchableOpacity>
        )}
      </View>
    );
  }

  renderSeparator() {
    return <View style={styles.item_seperator}></View>;
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.title_wrapper}>
          <TextS0
            family="demi"
            size="14"
            color={Global.C_SUBTEXT}
            label={title}
            textstyle={{ textAlign: 'center' }}
          />
        </View>

        <FlatList
          data={Items}
          renderItem={({ item, index }) => this.renderListItem(item, index)}
          ItemSeparatorComponent={this.renderSeparator}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: Global.ScreenWidth,
    alignItems: 'center',
  },
  title_wrapper: {
    width: '70%',
    alignItems: 'center',
    marginBottom: 30,
  },
  item_container: {
    width: '90%',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  item_imagewrapper: {
    width: '45%',
    aspectRatio: 1,
  },
  wh100: {
    width: '100%',
    height: '100%',
  },
  item_check: {
    position: 'absolute',
    bottom: 0,
    width: CheckHeight,
    height: CheckHeight,
    borderRadius: Math.round(CheckHeight / 2),
    justifyContent: 'center',
    alignItems: 'center',
  },
  item_text: {
    width: '45%',
    flexDirection: 'column',
    alignItems: 'center',
  },
  item_seperator: {
    height: 10,
  },
});
