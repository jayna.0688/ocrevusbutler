import React from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  Modal,
  ScrollView,
  FlatList,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import isEmpty from '@utils/isEmpty';
import {
  saveData,
  getData,
  deleteOneData,
  getAllData,
  deleteAllData,
  formatDateToString,
  formatTimeToString,
  formatDateFromSlash,
} from '@utils/GlobalFunction';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import RNRestart from 'react-native-restart';
import MenuIcon from '@component/MenuIcon';
import DateBar from './DateBar';
import InstructionView from './InstructionView';
import NotificationConfirmView from './NotificationConfirmView';
import AddNewView from './AddNewView';
import PointEditView from './PointEditView';
import TreatLineView from './TreatLineView';
import OverView from './OverView';
import MaterialCommunityIconsIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import RectShadowImage from '@component/RectShadowImage';
import Moment from 'moment';
import Strings from '@utils/Strings';
import { ShowNotification } from '@utils/Notification';
const ModalTop = Math.round(Global.ScreenHeight * 0.12);
const modalItems = [
  'Termine',
  'Dokumente',
  'Therapietag',
  'Über OCREVUS®',
  'Einstellungen',
  '--- Test Init ---',
  'Test pushnotification',
];
const menuIconRightMargin = 30;
const AddNewBtnHeight = Math.round(Global.ScreenWidth * 0.12);
const ItemImages = [
  require('@images/inject.png'),
  require('@images/flask.png'),
  '',
];
const TestObj = {
  1: {
    time: '0830',
    desc:
      'Here’s a checklist of the required tests to be done before your treatment. Please fill it in with your doctor.',
  },
};

const GoTodayHeight = 30;

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      todayString: '',

      datebar_dates: [], // array 3 '11/24/2019'
      datebar_index: 0, // 0~2

      points: {},
      // {
      //   '20191127' : {
      //     1: {  time: '',  desc: ''  },
      //     2: {  time: '',  desc: ''  },
      //   }
      // }

      pointDates: [],
      pointDatas: {},

      listOfOneDay: [],

      modalVisible: false,
      showInstruction: false,
      showNotificationConfirm: false,
      showAddNew: false,
      showOverview: false,
      initTreatlineSelect: false,
      showPointEditView: false,
      editObj: {
        date: '',
        item: {},
      },

      loading: true,
    };
    this.onChangeDateIndex = this.onChangeDateIndex.bind(this);
    this.onChangeDateArrow = this.onChangeDateArrow.bind(this);
    this.onInstructionFinish = this.onInstructionFinish.bind(this);
    this.onNotificationConfirmFinish = this.onNotificationConfirmFinish.bind(
      this,
    );
    this.pressToday = this.pressToday.bind(this);
    this.onAddNewClose = this.onAddNewClose.bind(this);
    this.onAddNewSave = this.onAddNewSave.bind(this);
    this.onEditViewClose = this.onEditViewClose.bind(this);
    this.onEditViewDelete = this.onEditViewDelete.bind(this);
    this.onEditViewSave = this.onEditViewSave.bind(this);
    this.onTreatlineOverView = this.onTreatlineOverView.bind(this);
  }

  async componentDidMount() {
    const passGuide = await getData('passGuide'); // instruction check
    if (passGuide !== '1') {
      // first
      this.setState({
        showInstruction: true,
      });
    } else {
      // passed guide
      let points_str = (await getData('points')) || '';
      if (points_str !== '') {
        points_str = points_str.substring(0, points_str.length - 1); // remove last ':'
        const pointDateArray = points_str.split(':');
        let points = {};
        for (let i = 0; i < pointDateArray.length; i++) {
          const onedate = pointDateArray[i];
          const tempstr = await getData(onedate);
          const onedate_value = JSON.parse(tempstr);

          points[onedate] = onedate_value;
        }
        this.setState({
          points: points,
        });
      }
    }
    this.goToday();
  }

  goToday() {
    const today = Moment().format('L');
    const tomorrow = Moment()
      .add(1, 'days')
      .format('L');
    const aftertomorrow = Moment()
      .add(2, 'days')
      .format('L');
    const todayString = formatDateFromSlash(today);
    this.setState({
      datebar_dates: [today, tomorrow, aftertomorrow],
      datebar_index: 0,

      todayString: todayString,
      loading: false,
    });
    if (this.state.showInstruction) {
      let points = {};
      points[formatDateFromSlash(today)] = TestObj;
      this.setState({
        points: points,
      });
    }
  }
  goDate(date) {
    //20191130
    const change_date =
      date.substring(4, 6) +
      '-' +
      date.substring(6, 8) +
      '-' +
      date.substring(0, 4);
    const first = Moment(change_date, 'MM-DD-YYYY').format('L');
    const second = Moment(change_date, 'MM-DD-YYYY')
      .add(1, 'days')
      .format('L');
    const third = Moment(change_date, 'MM-DD-YYYY')
      .add(2, 'days')
      .format('L');
    this.setState({
      datebar_dates: [first, second, third],
      datebar_index: 0,
    });
  }

  componentWillUnmount() {
    this.setState({ modalVisible: false });
  }

  pressModalItem(index) {
    // 0~5
    this.setState({ modalVisible: false });
    if (index === 0) {
      this.props.navigation.navigate('AppointmentsScreen');
    } else if (index === 1) {
      this.props.navigation.navigate('DocumentsScreen');
    } else if (index === 2) {
      this.props.navigation.navigate('TherapyDayScreen');
    } else if (index === 3) {
      this.props.navigation.navigate('UberOcrevusScreen');
    } else if (index === 4) {
      this.props.navigation.navigate('EinstellungenScreen');
    } else if (index === 5) {
      // index === 5
      deleteAllData();
      RNRestart.Restart();
    } else if (index === 6) {
      // test-pushnotification
      // ShowNotification();
    }
  }

  goBackOnBoarding() {
    // test
    deleteAllData();
    RNRestart.Restart();
  }

  pressItemImage(item) {

    const navigateAction = NavigationActions.navigate({
      routeName: 'ChecklistScreen',
      params: {
        type: parseInt(item.type, 10),
      },
    });
    this.props.navigation.dispatch(navigateAction);
  }

  pressPencil(item) {
    const currentSlashDate = this.state.datebar_dates[this.state.datebar_index];
    this.setState({
      showPointEditView: true,
      editObj: {
        date: currentSlashDate,
        item: item,
      },
    });
  }

  renderOneDayListItem(item) {
    // {type: "1", time: "0830", desc: "dsfsfs"}
    const type = item.type;
    const title =
      type === '1' ? 'Impfung' : type === '2' ? 'Blutuntersuchung' : 'Other...';

    const time_str = item.time;
    const time =
      time_str.substring(0, 2) + ':' + time_str.substring(2, 4) + ' Uhr';

    const desc = item.desc;

    return (
      <View style={styles.content_subwrapper}>
        <View style={styles.content_header}>
          {/* time and title */}
          <View style={styles.content_header_text}>
            <TextS1
              family="reg"
              size="11"
              color={Global.C_PERIWINKLE}
              label={time}
            />
            <TextS1
              family="bold"
              size="15"
              color={Global.C_PERIWINKLE}
              label={title}
              textstyle={{ marginTop: 5 }}
            />
          </View>

          {/* pencil */}
          <View style={styles.content_header_pencilwrapper}>
            <TouchableOpacity
              onPress={this.pressPencil.bind(this, item)}
              style={styles.content_header_pencil}>
              <MaterialCommunityIconsIcon
                name="pencil"
                size={20}
                color={'white'}
              />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.content_imagewrapper}>
          <View style={styles.content_imagesubwrapper}>
            <RectShadowImage
              onPress={() => this.pressItemImage(item)}
              width={Math.round(Global.ScreenWidth * 0.2688)}
              image={ItemImages[type - 1]}
              backcolor={Global.C_PERIWINKLE}
            />
          </View>

          <View style={styles.content_textwrapper}>
            <TextS0
              family="reg"
              size="12"
              color={Global.C_PERIWINKLE}
              label={desc}
              textstyle={styles.content_text}
            />
          </View>
        </View>
      </View>
    );
  }

  renderMenuModal() {
    return (
      <Modal
        visible={this.state.modalVisible}
        animationType={'fade'}
        onRequestClose={() => {
          this.state.modalVisible(false);
        }}
        transparent
        style={{ flex: 1 }}>
        <TouchableOpacity
          onPress={() => {
            this.setState({ modalVisible: false });
          }}
          style={styles.modal_screenwrapper}>
          <View style={styles.modal_wrapper}>
            {modalItems.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  onPress={this.pressModalItem.bind(this, index)}
                  style={styles.modal_itemwrapper}>
                  <TextS1
                    family="demi"
                    size="14"
                    color={Global.C_SUBTEXT}
                    label={item}
                  />
                </TouchableOpacity>
              );
            })}
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }

  onChangeDateIndex(index) {
    // 0~2
    this.setState({
      datebar_index: index,
      showOverview: false,
      initTreatlineSelect: true,
    });
    const selectDate = this.state.datebar_dates[index];
  }

  onChangeDateArrow(isNext) {
    const currentFirstDate = this.state.datebar_dates[0]; // '11/23/2019'
    const strDate =
      currentFirstDate.substring(6, 10) +
      currentFirstDate.substring(0, 2) +
      currentFirstDate.substring(3, 5);
    const firstMomentDate = Moment(strDate, 'YYYYMMDD');
    let fdate;
    if (isNext) {
      fdate = firstMomentDate.add(3, 'days').format('L');
    } else {
      fdate = firstMomentDate.subtract(3, 'days').format('L');
    }
    const sdate = firstMomentDate.add(1, 'days').format('L');
    const tdate = firstMomentDate.add(1, 'days').format('L');
    this.setState({
      datebar_dates: [fdate, sdate, tdate],
      showOverview: false,
      initTreatlineSelect: true,
    });
  }

  onInstructionFinish() {
    this.setState({
      showInstruction: false,
      points: {},
    });

    setTimeout(() => {
      this.setState({
        showNotificationConfirm: true,
      });
    }, 1000);
  }

  onNotificationConfirmFinish() {
    this.setState({
      showNotificationConfirm: false,
    });
  }

  pressToday() {
    this.goToday();
  }

  onAddNewClose() {
    this.setState({
      showAddNew: false,
    });
  }

  async onAddNewSave(date, type, time, desc) {
    let pointObj = {};
    pointObj[type] = {
      time: time,
      desc: desc,
    };
    let storage_points = (await getData('points')) || '';
    if (storage_points.indexOf(`${date}`) === -1) {
      storage_points += date + ':';
    }
    saveData('points', storage_points);

    const onepoint = await getData(date);

    if (onepoint === undefined) {
      saveData(date.toString(), JSON.stringify(pointObj));
    } else {
      let onepointObj = JSON.parse(onepoint);
      onepointObj = { ...onepointObj, ...pointObj };
      saveData(date.toString(), JSON.stringify(onepointObj));
    }

    const points = this.state.points;
    if (points[date]) {
      points[date] = {
        ...points[date],
        ...pointObj,
      };
    } else {
      points[date] = pointObj;
    }
    this.setState({
      points: points,
      showAddNew: false,
    });
  }

  onEditViewClose() {
    this.setState({
      showPointEditView: false,
    });
  }

  async onEditViewDelete(item) {
    let points = this.state.points;
    const currentDate = formatDateFromSlash(
      this.state.datebar_dates[this.state.datebar_index],
    );
    const currentData = points[currentDate];

    if (Object.keys(currentData).length === 1) {
      let store_points = await getData('points');
      const indexOf = store_points.indexOf(currentDate);
      const new_store_points =
        store_points.substring(0, indexOf) +
        store_points.substring(indexOf + 9, points.length) +
        '';
      saveData('points', new_store_points);
      delete points[currentDate];
    } else {
      delete points[currentDate][item.type];
      saveData(currentDate, JSON.stringify(points[currentDate]));
    }
    this.setState({
      points: points,
      showPointEditView: false,
    });
  }

  async onEditViewSave(date, type, time, desc) {
    let points = this.state.points;
    const currentDate = formatDateFromSlash(
      this.state.datebar_dates[this.state.datebar_index],
    );
    let currentData = points[currentDate];

    if (currentDate === date) {
      currentData[type] = {
        time: time,
        desc: desc,
      };
      saveData(currentDate, JSON.stringify(currentData));
      points[currentDate] = currentData;
    } else {
      this.onEditViewDelete({ type: type });
      this.onAddNewSave(date, type, time, desc);
    }
    this.setState({
      points: points,
      showPointEditView: false,
    });
  }

  goVaccinationBloodDay(isVaccination) {
    const points = this.state.points;
    const points_dates = Object.keys(points);
    let goDate = '';
    for (let i = 0; i < points_dates.length; i++) {
      const element_date = points_dates[i]; // date
      const oneday_point = points[element_date];
      if (isVaccination) {
        if (oneday_point['1']) {
          goDate = element_date;
          break;
        }
      } else {
        if (oneday_point['2']) {
          goDate = element_date;
          break;
        }
      }
    }

    if (goDate !== '') {
      this.goDate(goDate);
    }
  }

  onTreatlineOverView(index) {
    this.setState({
      showOverview: false,
      initTreatlineSelect: false,
    });
    if (index === 0) {
      this.goVaccinationBloodDay(true);
    } else if (index === 1) {
      this.goVaccinationBloodDay(false);
    } else if (index === 2) {
      this.setState({
        showOverview: true,
      });
    } else {
      this.props.navigation.navigate('TherapyDayScreen');
    }
  }

  render() {
    const {
      datebar_dates,
      datebar_index,
      points,
      loading,
      showInstruction,
    } = this.state;
    if (loading)
      return (
        <View style={{ flex: 1, backgroundColor: Global.C_PERIWINKLE }}></View>
      );

    const currentDate = formatDateFromSlash(datebar_dates[datebar_index]);
    let isToday = false;
    if (currentDate === this.state.todayString) isToday = true;

    const hasData = !isEmpty(points[currentDate]);
    let currentDataArray = [];
    if (hasData) {
      const current_point = points[currentDate];
      const current_point_keys = Object.keys(current_point);
      for (let i = 0; i < current_point_keys.length; i++) {
        const element = current_point_keys[i]; // 1 or 2 or 3
        const tempObj = {
          type: element,
          ...current_point[element],
        };
        currentDataArray.push(tempObj);
      }
    }

    const points_dates = Object.keys(points);

    return (
      <View style={styles.container}>
        {/* instruction view - absolute */}
        {showInstruction && (
          <InstructionView onFinish={this.onInstructionFinish} />
        )}

        {/* NotificationConfirm view - absolute */}
        {this.state.showNotificationConfirm && (
          <NotificationConfirmView
            onFinish={this.onNotificationConfirmFinish}
          />
        )}

        {/* add new view  - absolute*/}
        {this.state.showAddNew && (
          <AddNewView
            isToday={isToday}
            onClose={this.onAddNewClose}
            onSave={this.onAddNewSave}
            initDate={this.state.datebar_dates[this.state.datebar_index]}
          />
        )}

        {/* edit view - absolute */}
        {this.state.showPointEditView && (
          <PointEditView
            editObj={this.state.editObj}
            isToday={isToday}
            onClose={this.onEditViewClose}
            onDelete={this.onEditViewDelete}
            onSave={this.onEditViewSave}
          />
        )}

        {/* menu modal - absolute */}
        {this.renderMenuModal()}

        {/* gotoday and menu icon */}
        <View style={styles.menuicon_wrapper}>
          {isToday ? (
            <View></View>
          ) : (
              // go today
              <TouchableOpacity
                onPress={this.pressToday.bind(this)}
                style={styles.gotoday_wrapper}>
                <EntypoIcon name="chevron-small-left" size={20} color={'white'} />
                <TextS0
                  family="demi"
                  size="10"
                  color={'white'}
                  label={'  HEUTE  '}
                  textstyle={{ lineHeight: GoTodayHeight }}
                />
              </TouchableOpacity>
            )}

          <View>
            <MenuIcon
              onPress={() => this.setState({ modalVisible: true })}
              color={'white'}
            />
          </View>
        </View>

        {/* today string */}
        {isToday ? (
          <View style={styles.todaytext_wrapper}>
            <TextS0 family="reg" size="22" color="white" label="Heute" />
            <TextS0
              family="reg"
              size="14"
              color="white"
              label="31 Tage bis zu Deiner Therapie"
              textstyle={styles.mt10}
            />
          </View>
        ) : (
            <View style={styles.todaytext_change}></View>
          )}

        {/* datebar */}
        <View style={styles.datebar_wrapper}>
          {!loading && (
            <DateBar
              pointDates={points_dates}
              dates={this.state.datebar_dates}
              pickIndex={this.state.datebar_index}
              onChangeDateIndex={this.onChangeDateIndex}
              onChangeDateArrow={this.onChangeDateArrow}
            />
          )}
        </View>

        {/* content area */}
        <View style={[styles.content_wrapper, { flex: isToday ? 0.48 : 0.56 }]}>
          <ScrollView>
            <View style={styles.h40}></View>

            {this.state.showOverview ? (
              <OverView />
            ) : (
                <View style={{ width: '100%' }}>
                  {hasData && (
                    <FlatList
                      data={currentDataArray}
                      renderItem={({ item }) => this.renderOneDayListItem(item)}
                      keyExtractor={(item, index) => index.toString()}
                    />
                  )}

                  {/* no appointments */}
                  {!hasData && (
                    <View style={styles.noappoint}>
                      <TextS1
                        family="demi"
                        size="14"
                        color={Global.C_SUBTEXT}
                        label="Noch keine Termine"
                      />
                    </View>
                  )}

                  {/* add new */}
                  <TouchableOpacity
                    onPress={() => this.setState({ showAddNew: true })}
                    style={styles.addnew_wrapper}>
                    <TextS1
                      family="demi"
                      size="15"
                      color="white"
                      label={Strings.AddNew}
                      textstyle={{ lineHeight: AddNewBtnHeight }}
                    />
                  </TouchableOpacity>
                </View>
              )}

            {/* spacer */}
            <View style={{ height: 40 }}></View>
          </ScrollView>
        </View>

        {/* points view */}
        <View style={styles.f_013}>
          <TreatLineView
            onOverView={this.onTreatlineOverView}
            initTreatlineSelect={this.state.initTreatlineSelect}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column',
  },
  addnew_wrapper: {
    marginTop: 30,
    alignSelf: 'center',
    width: '70%',
    height: AddNewBtnHeight,
    backgroundColor: Global.C_PERIWINKLE,
    borderRadius: 5,
    // elevation: 10,
    // shadowColor: 'gray',
    // shadowOpacity: 0.5,
    // shadowOffset: { width: 0, height: 5 },
    // shadowRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modal_screenwrapper: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#ffffff88',
  },
  modal_wrapper: {
    position: 'absolute',
    top: ModalTop,
    right: 40,
    backgroundColor: 'white',
    borderRadius: 5,
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 10,
    paddingHorizontal: 20,
    elevation: 10,
    shadowColor: 'gray',
    shadowOpacity: 0.8,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 20,
  },
  modal_itemwrapper: {
    width: Math.round(Global.ScreenWidth * 0.35),
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,
  },
  f_013: {
    flex: 0.13,
  },
  noappoint: {
    marginTop: 80,
    alignSelf: 'center',
  },
  content_wrapper: {
    backgroundColor: 'white',
    zIndex: 3,
    flexDirection: 'column',
  },
  h40: {
    height: 40,
  },
  datebar_wrapper: {
    flex: 0.13,
    zIndex: 5,
  },
  todaytext_wrapper: {
    flex: 0.14,
    backgroundColor: Global.C_PERIWINKLE_LIGHT,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 50,
  },
  mt10: {
    marginTop: 10,
  },
  menuicon_wrapper: {
    flex: 0.12,
    backgroundColor: Global.C_PERIWINKLE_LIGHT,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingHorizontal: menuIconRightMargin,
  },
  gotoday_wrapper: {
    height: GoTodayHeight,
    borderRadius: 5,
    borderColor: 'white',
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  content_subwrapper: {
    width: '100%',
    flexDirection: 'column',
    marginBottom: 30,
  },
  content_header: {
    height: Math.round(Global.ScreenWidth * 0.12),
    paddingLeft: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  content_header_text: {
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
  },
  content_header_pencilwrapper: {
    height: '100%',
    paddingRight: 30,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  content_header_pencil: {
    height: '70%',
    aspectRatio: 1,
    backgroundColor: Global.C_PERIWINKLE,
    borderRadius: Math.round(Global.ScreenWidth * 0.042),
    elevation: 10,
    shadowColor: 'gray',
    shadowOpacity: 0.5,
    shadowOffset: { width: 0, height: 5 },
    shadowRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content_imagewrapper: {
    paddingLeft: 50,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  todaytext_change: {
    flex: 0.06,
    backgroundColor: Global.C_PERIWINKLE_LIGHT,
  },
  content_imagesubwrapper: {
    marginTop: 10,
    marginBottom: 20,
  },
  content_textwrapper: {
    marginTop: 20,
    width: Math.round(Global.ScreenWidth - Global.ScreenWidth * 0.2688 - 90),
  },
  content_text: {
    marginLeft: 15,
    textAlign: 'justify',
    lineHeight: 14,
  },
});
